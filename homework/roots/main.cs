using System;
using static System.Math;
using static System.Console;
using System.IO;

public static class main{
    public static void Main(){
        double ε2 = Pow(2,-26);
        WriteLine($"Test with Rosenbrock's valley function");
        double[,] RB_doublematrix = {{ 1.0, 1.0, 1.2, 0.6, 0.7},
                                      { 1.0, 1.2, 2.0, 0.7, 1.2}};
        matrix RB_matrix = new matrix(RB_doublematrix);
        Func<vector, double> RB_f = (t) => {
            double y = Pow(1 - t[0], 2) + 100 * Pow(t[1] - t[0] * t[0], 2);
            return y;
        };
        Func<vector, vector> RB_df = (t) => {
            vector y = new vector(t.size);
            y[0] = 2 * (t[0] - 1) + 400 * (t[0] * t[0] * t[0] - t[1] * t[0]);
            y[1] = 200 * (t[1] - t[0] * t[0]);
            return y;
        };

        for(int i = 0; i < RB_matrix.size1; i++){
            vector RB_x = RB_matrix[i];
            roots RB = new roots(RB_df);
            vector RB_answer = RB.newton(RB_x, -1, ε2);
            vector RB_dfunc = RB_df(RB_answer);
            double RB_func = RB_f(RB_answer);
            matrix RB_area = new matrix(2, 360);
            for(int θ = 0; θ < 360; θ++){
                vector angle = new vector(ε2 * Cos(θ), ε2 * Sin(θ));
                RB_area[0, θ] = RB_f(RB_answer + angle);
                RB_area[1, θ] = θ;
            } 
            int RB_min_index = RB_area[0].min();
            int RB_max_index = RB_area[0].max();
            
            bool RB_Test = (RB_area[0, RB_min_index] != RB_func && RB_area[0, RB_max_index] != RB_func);
            vector RB_xy_max = RB_answer + new vector(ε2 * Cos(RB_max_index), ε2 * Sin(RB_max_index));
            vector RB_xy_min = RB_answer + new vector(ε2 * Cos(RB_min_index), ε2 * Sin(RB_min_index));
            WriteLine($"_______________");
            WriteLine("Initial guess:");
            RB_x.print();
            WriteLine($"\nAfter {RB.gange} iterations");
            WriteLine($"roots of the gradient were found to be:");
            RB_answer.print();
            WriteLine($"\ndf(x):");
            RB_dfunc.print();
            WriteLine($"\nThe roots gives: f => {RB_func}");
            WriteLine($"\nTesting if surrounding area both goes lower and higher than this point.");
            WriteLine($"\n{RB_Test}.\nHigher at:");
            RB_xy_max.print();
            WriteLine($"\nand lower at:");
            RB_xy_min.print();
            WriteLine();
        }
        WriteLine($"Test with Himmelblau's function");
        double[,] HB_doublematrix = {{ 2.8,-2.7,-3.6, 3.7},
                                      { 2.2,-3.0,-3.3,-1.6}};
        matrix HB_matrix = new matrix(HB_doublematrix);
        Func<vector, double> HB_f = (t) => {
            double y = Pow(t[0] * t[0] + t[1] - 11, 2) + Pow(t[0] + t[1] * t[1] - 7, 2);
            return y;
        };
        Func<vector, vector> HB_df = (t) => {
            vector y = new vector(t.size);
            y[0] = 2 * (2 * t[0] * (t[0] * t[0] + t[1] - 11) + t[0] + t[1] * t[1] - 7);
            y[1] = 2 * (t[0] * t[0] + 2 * t[1] * (t[0] + t[1] * t[1] - 7) + t[1] -11);
            return y;
        };
        for(int i = 0; i < HB_matrix.size1; i++){
            vector HB_x = HB_matrix[i];
            roots HB = new roots(HB_df);
            vector HB_answer = HB.newton(HB_x, -1, ε2);
            vector HB_dfunc = HB_df(HB_answer);
            double HB_func = HB_f(HB_answer);
            matrix HB_area = new matrix(2, 360);
            for(int θ = 0; θ < 360; θ++){
                vector angle = new vector(ε2 * Cos(θ), ε2 * Sin(θ));
                HB_area[0, θ] = HB_f(HB_answer + angle);
                HB_area[1, θ] = θ;
            } 
            int HB_min_index = HB_area[0].min();
            
            bool HB_Test = (HB_area[0, HB_min_index] > HB_func);
            vector HB_xy_min = HB_answer + new vector(ε2 * Cos(HB_min_index), ε2 * Sin(HB_min_index));
            WriteLine($"_______________");
            WriteLine("Initial guess:");
            HB_x.print();
            WriteLine($"\nAfter {HB.gange} iterations");
            WriteLine($"roots of the gradient were found to be:");
            HB_answer.print();
            WriteLine($"\ndf(x):");
            HB_dfunc.print();
            WriteLine($"\nThe roots gives: f => {HB_func}");
            WriteLine($"\nTesting if surrounding area both goes lower and higher than this point.");
            WriteLine($"\n{HB_Test}.");
            WriteLine($"\nLower than 2nd-lowest point:");
            HB_xy_min.print();
            WriteLine();
        }
        WriteLine($"Bound states of hydrogen atom.");
        for(int j = 2; j < 10; j++){
            bool old = true;
            Func<vector, vector> yvector = (y) => {
                double E = y[0];
                double Fe = hydrogen.Fe(E, j);
                return new vector(Fe);
            };
            vector Bohr_x = new vector(-1.0);
            roots Bohr = new roots(yvector);
            vector Bohr_answer = Bohr.newton(Bohr_x, -1, 1e-4);
            double energy = Bohr_answer[0];
            if(j == 2){
                old = false;
            }
            if(j == 8){
                WriteLine($"_______________");
                WriteLine("Initial guess:");
                Bohr_x.print();
                WriteLine($"\nAfter {Bohr.gange} iterations");
                WriteLine($"root for M(E) = 0 is found to be at:");
                Bohr_answer.print();
            }
            using (StreamWriter data = new StreamWriter("exact.txt", old)){
                for(double r = 0; r <= 11; r+=0.1){
                    data.Write($"{r} {hydrogen.Fe(energy, r)}  {r*Exp(-r)}");
                    data.Write($"\n");
                    }
                data.Write($"\n");
                data.Write($"\n");
                }
            using (StreamWriter data = new StreamWriter("calc.txt", old)){
                    data.Write($"{j} {energy}");
                    data.Write($"\n");
                }
        }
    }
}