using System;
using static System.Math;
using static System.Console;
using System.IO;

public static class main{
    public static void Main(){
        vector a = new vector(0,0);
        vector b = new vector(1,1);
        int N;
        double answer, error;
        Func<vector,double> circlefunc = (t) => {
            double x = t[0];
            double y = t[1];
            if(x * x + y * y <= 1){
                return 1;
            }
            return 0.0;
        };
        int gange = 1000;
        matrix info = new matrix(3, gange);
        N = 100;
        for(int i = 0; i < gange; i++){
            N += 100;
            (answer, error) = monte_carlo.plainmc(circlefunc, a, b, N);
            info[i] = new vector(N, answer, error);
        }

        using (StreamWriter circledatafile = new StreamWriter("circle.txt", false))
        {
            for(int i = 0; i < gange; i++){
                int Ni = (int)info[i][0];
                double answeri = info[i][1];
                double errori = info[i][2];
                circledatafile.Write($"{Ni}, {answeri}, {errori}, {Abs(answeri - PI/4)}");
                circledatafile.Write("\n");
            }
        }
        N = (int)info[gange-1][0];
        answer = info[gange-1][1];
        error = info[gange-1][2];
        Write($"Circle integral (N: {N}, answer: {answer}, error:{error})\n");
        
        a = new vector(0,0,0);
        b = new vector(PI,PI,PI);
        Func<vector,double> func = (t) => {
            double x = t[0];
            double y = t[1];
            double z = t[2];
            return 1/(1 - Cos(x) * Cos(y) * Cos(z))/Pow(PI,3);
        };
        double exact = 1.3932039296856768591842462603255;
        gange = 1000;
        info = new matrix(3, gange);
        N = 100;
        for(int i = 0; i < gange; i++){
            N += 100;
            (answer, error) = monte_carlo.plainmc(func, a, b, N);
            info[i] = new vector(N, answer, error);
        }

        using (StreamWriter funcdatafile = new StreamWriter("func.txt", false))
        {
            for(int i = 0; i < gange; i++){
                int Ni = (int)info[i][0];
                double answeri = info[i][1];
                double errori = info[i][2];
                funcdatafile.Write($"{Ni}, {answeri}, {errori}, {Abs(answeri - exact)}");
                funcdatafile.Write("\n");
            }
        }
        N = (int)info[gange-1][0];
        answer = info[gange-1][1];
        error = info[gange-1][2];
        Write($"Difficult integral (N: {N}, answer: {answer}, error:{error})\n");
    }
}