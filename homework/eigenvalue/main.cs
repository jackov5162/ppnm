using System;
using static System.Console;
using System.IO;
using System.Text;
public static class main{
	public static void Main(){
		WriteLine($"_______________________________________________\n");
		WriteLine($"Jacobi diagonalization with cyclic sweeps:\n");
		int size1 = 3;
		int size2 = 3;
		matrix A = random_sym_matrix(size1, size2);
		EVD Jacobi = new EVD(A);
		matrix V = Jacobi.V;
		matrix VT = Jacobi.VT;
		vector w = Jacobi.w;
		matrix D = new matrix(w);
		matrix VDVT = V * D * VT;
		matrix VTAV = VT * A * V;
		matrix I = V.I;
		matrix VVT = V * VT;
		matrix VTV = VT * V;
		A.print("A");
		WriteLine($"\n");
		V.print("V");
		WriteLine($"\n");
		VT.print($"V^T");
		WriteLine($"\n");
		D.print("D");
		WriteLine($"\n");
		w.print("w");
		WriteLine($"\n");
		VDVT.print($"VDV^T");
		WriteLine($"\n");
		WriteLine($"VDV^T is equal to A: {VDVT.approx(A)}\n");
		VTAV.print($"V^TAV");
		WriteLine($"\n");
		WriteLine($"V^TAV is equal to D: {VTAV.approx(D)}\n");
		WriteLine($"V * V^T is equal to V^T * V: {VVT.approx(VTV)}\n");
		WriteLine($"V * V^T is a identity matrix: {VVT.approx(I)}\n");
		WriteLine($"\n");

	}
	public static matrix random_matrix(int size1, int size2){
		matrix mat = new matrix(size1,size2);
		var rnd = new System.Random(1);
		for(int i = 0; i < size1; i++){
			for(int j = 0; j < size2; j++){
				mat[i,j] = rnd.NextDouble();
			}
		}
		return mat;
	}

	public static matrix random_sym_matrix(int size1, int size2){
		matrix mat = new matrix(size1,size2);
		var rnd = new System.Random(1);
		for(int i = 0; i < size1; i++){
			for(int j = i; j < size2; j++){
				double number = rnd.NextDouble();
				mat[i,j] = number;
				mat[j,i] = number;
			}
		}
		return mat;
	}

	public static vector random_vector(int size1){
		vector vec = new vector(size1);
		var rnd = new System.Random(2);
		for(int i = 0; i < size1; i++){
			vec[i] = rnd.NextDouble();
		}
		return vec;
	}
}