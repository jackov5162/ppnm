using System;
using static System.Math;
using static System.Console;
using System.IO;

public static class main{
    public static void Main(){
        Func<double,double> g = x => {
            return Cos(5 * x - 1) * Exp(-x * x);
        };
        vector xs = new vector(-1, -0.8, -0.6, -0.4, -0.2, 0, 0.2, 0.4, 0.6, 0.8, 1);
        vector ys = new vector(xs.size);
        for(int i = 0; i < xs.size; i++){
            ys[i] = g(xs[i]);
        }
        int neurons = 40;
        ann brain = new ann(neurons);
        brain.train(xs, ys);
        vector p_init = brain.initial_p;
        vector p = brain.p;
        vector ys_est = new vector(xs.size);
        for(int i = 0; i < xs.size; i++){
            ys_est[i] = brain.response(xs[i], p);
        }
        using (StreamWriter neural = new StreamWriter("neuralnet.txt", false))
        {
            for(int i = 0; i < xs.size; i++){
                neural.Write($"{xs[i]} {ys_est[i]}\n");
            }
        }
        using (StreamWriter answer = new StreamWriter("Out.txt", false))
        {
            Write($"____________________________\n");
            Write($"Neural Network:\n");
            Write($"\n");
            xs.print("x values");
            Write($"\n");
            ys.print("y values");
            Write($"\n");
            p_init.print("Initial parameters");
            Write($"\n");
            p.print("Final parameters");
            Write($"\n");
            ys_est.print("Estimated y values");
        }

    }

}