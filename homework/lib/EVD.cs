using static System.Math;
using static System.Console;
public class EVD{
    public matrix A, V, VT;
    public vector w;
    public EVD(matrix M){
        A = M.copy();
        V = M.I;
        w = new vector(M.size1);
        bool changed;
        do{
            changed = false;
            for(int p = 0; p < M.size1 - 1; p++)
            for(int q = p + 1; q < M.size1; q++){
                double apq = A[p,q], app = A[p,p], aqq = A[q,q];
                double theta = 0.5 * Atan2(2 * apq, aqq - app);
                double c = Cos(theta), s = Sin(theta);
                double new_app = c * c * app - 2 * s *c * apq + s * s * aqq;
                double new_aqq = s * s * app + 2 * s *c * apq + c * c * aqq;
                if(new_app != app || new_aqq != aqq){
                    changed = true;
                    timesJ(A,p,q, theta);
                    Jtimes(A,p,q,-theta);
                    timesJ(V,p,q, theta);
                }
            }
        }
        while(changed);
        VT = V.T;
        for(int i = 0; i < M.size1; i++)
            w[i] = A[i,i];
    }
    public static void timesJ(matrix A, int p, int q, double theta){
        double c = Cos(theta);
        double s = Sin(theta);
        for(int i = 0; i < A.size1; i++){
            double Aip = A[i,p];
            double Aiq = A[i,q];
            A[i,p] =  c * Aip - s * Aiq;
            A[i,q] =  s * Aip + c * Aiq;
        }
    }
    public static void Jtimes(matrix A, int p, int q, double theta){
        double c = Cos(theta);
        double s = Sin(theta);
        for(int i = 0; i < A.size1; i++){
            double Api = A[p,i];
            double Aqi = A[q,i];
            A[p,i] =  c * Api + s * Aqi;
            A[q,i] = -s * Api + c * Aqi;
        }
    }
}