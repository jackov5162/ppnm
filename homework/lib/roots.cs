using System;
using static System.Console;
using static System.Math;

public class roots{
    public matrix J;
    public vector x, δx;
    public double λ = 1;
    public int gange = 0;
    public Func<vector,vector> func;

    public roots(Func<vector,vector> f){
        func = f;
    }
    public vector newton(vector initial_x, int maks_gange = -1, double ε=1e-2){
        gange = 0;
        x = initial_x;
        δx = x.absVec() * Sqrt(ε);
        J = new matrix(x.size);

        do{
            for(int i = 0; i < x.size; i++){
                for(int k = 0; k < x.size; k++){
                    vector only_xk = δx.onlyk(k);
                    if(δx[k] == 0){
                        J[i, k] = 1; /* solves 0/0 issue giving NaN */                                            
                    }else{
                        J[i, k] = (func(x + only_xk) - func(x))[i] / δx[k]; /* Calculate the Jacobian matrix J */ 
                    }                
                }
            }
            QRGS JJ = new QRGS(J);
            vector Δx = JJ.solve(-func(x)); /* solve JΔx = -f(x) for Δx */
            λ = 1;
            do{
                λ /= 2;
            }while(func(x + λ * Δx).norm() > (1 - λ / 2) * func(x).norm() && (λ * Δx).norm() >= δx.size);
            vector λΔx = λ * Δx;
            x += λΔx;
            gange += 1;
        }while(func(x).norm() > ε );
        return x;
    }
}