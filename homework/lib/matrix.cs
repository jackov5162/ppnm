using System;
using static System.Console;
using static System.Math;

public class matrix{
	public int size1; /* size1 is initially the rows, when making the matrix */
	public int size2; /* size2 is the corresponding columns */
	public double[][] data; /* The matrix itself */

	private double[][] datamaker(int n, int m){
		/* Creates a matrix from given integers of rows and columns. */
		data = new double[m][]; /* Creates the rows in the matrix. */
		for(int i = 0; i < m; i++)
			data[i] = new double[n]; /* Creates the columns in each row. */
		return data;
		}

	public matrix(int n, int m){
		/* Creates a matrix with "datamaker", given integers of rows and columns. */
		size1 = n;
		size2 = m;
		data = datamaker(size1,size2);
	}

	public matrix(int rect) : this(rect,rect) {
		/* Creates a rectangular matrix with a given integer length. */
	}

	public double this[int n, int m]{
		/* Getter and Setter for the doubles in the matrix.
		Getter: Given matrix[i,j]. It responds the element in j'th column and i'th row.
		Setter: Given matrix[i,j] = a. It sets the matrix[i,j] element to a.
		*/
		get{return data[m][n];}
		set{data[m][n] = value;}
	}

	public vector this[int c]{
		/* Getter and Setter for the rows in the matrix.
		Getter: Given Matrix[c]. Returns the c'th row as a vector. 
		Setter: Given Matrix[c] = v. It set the row in matrix[c] to the vector v.
		*/
		get{return (vector)data[c];}
		set{data[c] = (double[])value;}
	}

	public matrix(vector e): this(e.size,e.size){
		/* Creates a diagonal matrix with the element corresponding to the elements in vector e. */
		for(int diag = 0; diag < e.size; diag++)this[diag,diag] = e[diag];
	}

	public matrix(string[] strMatrix){
		/* Creating matrix from a list of strings. */
		size1 = strMatrix.Length; /* Sets the row size in the matrix. */
		size2 = strMatrix[0].Split(" ").Length; /* Set the column size. */
		data = datamaker(size1,size2);
		for(int i = 0; i < size1; i++){
			string[] numbers = strMatrix[i].Split(" ");/* Split each string up into numbers. */
			for(int j = 0; j < size2; j++){
				this[i,j] = Double.Parse(numbers[j]); /* change formate of numbers to doubles and insert. */
			}
		}
	}
	public matrix(double[,] doubleMatrix){
		/* Same process as with a list of strings, but no formate change. */
		size1 = doubleMatrix.GetLength(0);
		size2 = doubleMatrix.GetLength(1);
		data = datamaker(size1,size2);
		for(int i = 0; i < size1; i++)
			for(int j = 0; j < size2; j++)
				this[i,j] = doubleMatrix[i,j];
	}

	public matrix(string strMatrix){
		/* With a single string, which split into a list of strings using ';' as separetor.
		Same process as with a list of strings, but no formate change. */
		string[] lines = strMatrix.Split(" ; ");
		size1 = lines.Length;
		size2 = lines[0].Split(" ").Length;
		data = datamaker(size1,size2);
		for(int i = 0; i < size1; i++){
			string[] numbers = lines[i].Split(" ");
			for(int j = 0; j < size2; j++){
				this[i,j] = Double.Parse(numbers[j]);
			}
		}
	}

	public static matrix operator+ (matrix a, matrix b){
		/* Addition of matrix a and b. */
		matrix c = new matrix(a.size1,a.size2);
		for(int i = 0; i < a.size1; i++){
			for(int j = 0; j < a.size2; j++){
				c[i,j] = a[i,j] + b[i,j];
			}
		}
		return c;
	}

	public static matrix operator- (matrix a, matrix b){
		/* Subtraction of matrix a and b. */
		matrix c = new matrix(a.size1,a.size2);
		for(int i = 0; i < a.size1; i++){
			for(int j = 0; j < a.size2; j++){
				c[i,j] = a[i,j] - b[i,j];
			}
		}
		return c;
	}

	public static matrix operator- (matrix a){
		/* Makes the matrix negativ when '-' is infront. */
		matrix c = new matrix(a.size1,a.size2);
		for(int i = 0; i < a.size1; i++){
			for(int j = 0; j < a.size2; j++){
				c[i,j] = -a[i,j];
			}
		}
		return c;
	}

	public static matrix operator* (matrix a, matrix b){
		/* Multiplication between matrix a and b */
		matrix c = new matrix(a.size1,b.size2);
		for(int i = 0; i < a.size2; i++){
			for(int j = 0; j < b.size2; j++){
				for(int k = 0; k < a.size1; k++){
					c[k,j] += a[k,i] * b[i,j];
				}
			}
		}
		return c;
	}

	public static matrix operator* (matrix a, double x){
		/* Multiplication between a matrix 'a' and a double 'x'. */
		matrix c = new matrix(a.size1,a.size2);
		for(int i = 0; i < a.size1; i++){
			for(int j = 0; j < a.size2; j++){
				c[i,j] = a[i,j] * x;
			}
		}
		return c;
	}

	public static matrix operator* (double x, matrix a){
		/* Multiplication scenario for when the double is given first. */
		return a*x;
	}

	public static matrix operator/ (matrix a, double x){
		/* Division with a double 'x'. */
		matrix c = new matrix(a.size1,a.size2);
		for(int i = 0; i < a.size1; i++){
			for(int j = 0; j < a.size2; j++){
				c[i,j] = a[i,j] / x;
			}
		}
		return c;
	}

	public static vector operator* (matrix a, vector v){
		/* Multiplication with a vector 'v'. */
		var u = new vector(a.size1);
		for(int i = 0; i < a.size1; i++){
			for(int j = 0; j < a.size2; j++){
				u[i] += a[i,j] * v[j];
			}
		}
		return u;
	}

	public static vector operator% (matrix a, vector w){
		/* Multiplication with a vector 'w'. Here 'w' is 'v'^T. */
		var u = new vector(a.size2);
		for(int i = 0; i < a.size1; i++){
			for(int j = 0; j < a.size2; j++){
				u[j] += a[i,j] * w[i];
			}
		}
		return u;
	}

	public matrix T{
		get{return this.transpose();}
		set{}
	}
	public matrix I{
		get{return this.identity();}
		set{}
	}

	public matrix transpose(){
		/* The matrix transposed, meaning matrix^T. */
		matrix c = new matrix(size2,size1);
		for(int j = 0; j < size2; j++){
			for(int i = 0; i < size1; i++){
				c[j,i] = this[i,j];
				}
			}
		return c;
	}

	public matrix inverse(){
		/* The inverse of the matrix. */
		matrix c = this.identity();
		QRGS QR = new QRGS(this);
		matrix solution = new matrix(size1,size2);
		for(int i = 0; i < size1; i++)
			solution[i,i] = QR.solve(c[i])[i];
		return solution;
	}

	public matrix identity(){
		/* The corresponding identity matrix of the matrix. */
		matrix c = new matrix(size1,size2);
		for(int diag = 0; diag < size1; diag++)
			c[diag,diag] = 1;
		return c;
	}

	public matrix copy(){
		/* A copy of the matrix. */
		matrix copy_of = new matrix(size1,size2);
		for(int i = 0; i < size1; i++){
			for(int j = 0; j < size2; j++){
				copy_of[i,j] = this[i,j];
			}
		}
		return copy_of;
	}

	public matrix addcol(vector v){
		/* Adds the vector 'v' as a column in the matrix. */
		matrix expanded = new matrix(size1,size2 + 1);
		for(int i = 0; i < size1; i++){
			for(int j = 0; j < size2; j++){
				expanded[i,j] = this[i,j];
			}
			expanded[i,size2] = v[i];
		}
		return expanded;
	}

	public void print(string s="", string format="{0,10:g3} ", System.IO.TextWriter file=null){
		/* print the matrix. */
		if(file==null) file=System.Console.Out;
		file.WriteLine(s);
		for(int ir=0;ir<this.size1;ir++){
			for(int ic=0;ic<this.size2;ic++) file.Write(format,this[ir,ic]);
			file.WriteLine();
		}
	}
				
	public static bool approx(double a, double b, double acc = 1e-6, double eps = 1e-6){
		/* Boolian for if double 'a' is equal double 'b' within approximation. */
		if(Abs(a - b) < acc) return true;
		if(Abs(a - b) / Max(Abs(a), Abs(b)) < eps) return true;
		return false;
	}

	public bool approx(matrix B, double acc = 1e-6, double eps = 1e-6){
		/* Boolian for if the matrix is approximatly equal to matrix 'B'. */
		if(this.size1 != B.size1) return false;
		if(this.size2 != B.size2) return false;
		for(int i = 0; i < size1; i++)
			for(int j = 0; j < size2; j++)
				if(!approx(this[i,j], B[i,j], acc, eps))
					return false;
		return true;
	}
}
