using System;
using static System.Console;
using static System.Math;
using System.Collections.Generic;
public static class ODEDriver{
    public static (vector,matrix) driver(
        Func<double,vector,vector> F,
        (double,double) interval,
        vector ystart,
        double h=0.01,
        double acc=0.01,
        double eps=0.01,
        int nmax = 9999999
        ){
        int n = 0;
        var (a,b) = interval; double x=a; vector y=ystart.copy();
        vector xlist = new vector(1);
        xlist[0] = x;
        matrix ylist = new matrix(y.size, 1);
        for(int i = 0; i < y.size - 1; i++){
            ylist[0, i] = y[i];
        }
        do{
            if(x>=b) return (xlist,ylist);
            if(x+h>b) h=b-x;
            var (yh,dy) = RK_ODE.rkstep23(F, x, y, h);
            double tol = (acc+eps*yh.norm()) * Sqrt(h/(b-a));
            double err = dy.norm();
            if(err<=tol){
                x += h; y = yh;
                xlist = xlist.add(x);
                ylist = ylist.addcol(y);
            }
            h *= Min(Pow(tol/err,0.25)*0.95,2);
            n += 1;
        }while(n <= nmax);
        throw new Exception("Out of iterations");
    }
    public static Func<double,vector> make_ODE_ivp_interpolant(
        Func<double,vector,vector> f, 
        (double,double) interval, 
        vector ystart,
        double acc=0.01,
        double eps=0.01,
        double hstart=0.01,
        int nmax = 9999999
        ){
            (vector xlist, matrix ylist) = driver(f, interval, ystart, nmax = nmax);
            qspline_matrix_version ODE_interpolant = new qspline_matrix_version(xlist, ylist);
            return ODE_interpolant.evaluate();
        }
}


