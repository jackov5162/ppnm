using System;
using static System.Math;
public class cspline{
    public vector x, y, b, c, d, D, Q, B;
    public int bin, n;
    public cspline(vector xs, vector ys){
        x = xs.copy();
        y = ys.copy();
        n = x.size;
        b = new vector(n);
        c = new vector(n - 1);
        d = new vector(n - 1);
        D = new vector(n);
        Q = new vector(n - 1);
        B = new vector(n);

        vector p = new vector(n - 1);
        vector h = new vector(n - 1);
        for(int i = 0; i < n - 1; i++){
            h[i] = x[i + 1] - x[i];
            p[i] = (y[i + 1] - y[i]) / h[i];
        }

        D[0] = 2;
        D[n - 1] = 2;
        Q[0] = 1;
        B[0] = 3 * p[0];
        B[n - 1] = 3 * p[n - 2];
        for(int i = 0; i < n - 2; i++){
            D[i + 1] = 2 * h[i] / h[i + 1] + 2;
            Q[i + 1] = h[i] / h[i + 1];
            B[i + 1] = 3 * (p[i] + p[i + 1] * h[i] / h[i + 1]);
        }

        for(int i = 1; i < n; i++){
            D[i] -= Q[i - 1] / D[i - 1];
            B[i] -= B[i - 1] / D[i - 1];
        }

        b[n - 1] = B[n - 1] / D[n - 1];
        for(int i = n - 2; i >= 0; i--){
            b[i] = (B[i] - Q[i] * b[i + 1]) / D[i];
        }

        for(int i = 0; i < n - 1; i++){
            c[i] = (-2 * b[i] - b[i + 1] + 3 * p[i]) / h[i];
            d[i] = (b[i] + b[i + 1] - 2 * p[i]) / (h[i] * h[i]);
        }
    }
    public void binsearch(double z){
        /* locates the interval for z by bisection */
        if(z < x[0] || z > x[n - 1]){
            throw new Exception("binsearch: bad z");
        }
        bin = 0;
        int j = n - 1;
        while(j - bin > 1){
            int mid = (bin + j) / 2;
            if(z > x[mid]){
                bin = mid;
            }
            else{
                j = mid;
            }
        }
    }
    public double evaluate(double z){
        binsearch(z);
        double h = z - x[bin];
        return y[bin] + h * b[bin] + Pow(h, 2) * c[bin] + Pow(h, 3) * d[bin];
    }
    public double derivative(double z){
        binsearch(z);
        double h = z - x[bin];
        return b[bin] + h * 2 * c[bin] + 3 * Pow(h, 2) * d[bin];
    }
    public double integral(double z){
        binsearch(z);
        double sum = 0;
        double h;
        for(int i =0; i < bin; i++){
            h = x[i + 1] - x[i];
            sum += h * y[i] + Pow(h, 2) / 2 * b[i] + Pow(h, 3) / 3 * c[i] + Pow(h, 4) / 4 * d[i];
        }
        h = z - x[bin];
        sum += h * y[bin] + Pow(h, 2) / 2 * b[bin] + Pow(h, 3) / 3 * c[bin] + Pow(h, 4) / 4 * d[bin];
        return sum;
    }
}