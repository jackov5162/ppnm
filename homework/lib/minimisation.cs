using System;
using static System.Math;
using static System.Console;
using System.IO;

public class minimisation{
    private bool givesnan = false;
    public vector x, δx, Δφ;
    public matrix H;
    public Func<vector,double> φ;
    public int nsteps = 0;
    public double λ, Δφ_norm, limit, φx;
    public double ε = Pow(2, -26);

    public minimisation(Func<vector,double> f, double acc = -1){
        φ = f;     
        if(acc > ε) ε = acc;
    }

    public bool nanresult(){
        return givesnan;
    }

    public vector gradient(vector x){
        φx = φ(x);
        vector Δφx = new vector(x.size);
        for(int i = 0; i < x.size; i++){
            double dx = Max(Abs(x[i]), 1) * ε;
            x[i] += dx;
            Δφx[i] = (φ(x) - φx) / dx; /* Calculate vector gradient Δφ */ 
            x[i] -= dx;
            }
        return Δφx;
    }
    public matrix hessian(vector x, vector Δφx){
        H = new matrix(x.size);
        for(int i = 0; i < x.size; i++){
            double dx = Max(Abs(x[i]), 1) * Pow(2, -13);
            x[i] += dx;
            vector dΔφx = this.gradient(x) - Δφx;
            for(int k = 0; k < x.size; k++) H[i, k] = dΔφx[k] / dx; /* Calculate the Hessian matrix H */
            x[i] -= dx;
        }
	return H;
        //return (H + H.transpose()) / 2;
    }
    public vector newton(vector initial_x, double limit = 1e-4, double λ_min = 1e-3){
        givesnan = false;
        x = initial_x.copy();
        nsteps = 0;
        Δφ = this.gradient(x);
        do{
            Δφ_norm = Δφ.norm();
            if(double.IsNaN(Δφ_norm)){
                givesnan = true;
                break;
            }
            System.Console.Error.WriteLine($"grad norm = {Δφ_norm} limit = {limit}");
            if(Δφ_norm < limit) {
            	System.Console.Error.WriteLine($"-- newton: Job done --");
		        break;
	        }
            if(100000 < nsteps) {
            	System.Console.Error.WriteLine($"-- newton: failed --");
		        break;
	        }
            H = this.hessian(x, Δφ);
	        QRGS HQR = new QRGS(H);
            vector Δx = HQR.solve(-Δφ);
            λ = 1;
            do{
                if(φ(x + λ * Δx) < φ(x)) break;
                if( λ < λ_min) break;
                λ /= 2;
            }while(true);
            x += λ * Δx;
            Δφ = this.gradient(x);
            nsteps++;
        }while(true);
        return x;
    }
    public string extrema(){
        double func = φ(x);
        matrix area = new matrix(2, 360);
        for(int θ = 0; θ < 360; θ++){
            vector angle = new vector(Cos(θ), Sin(θ));
            area[0, θ] = φ(x + angle);
            area[1, θ] = θ;
        }
        int min_index = area[0].min();
        bool testmin = (area[0, min_index] > func);
        if(testmin) return "minimum";
        return "extremum";
        }
    }
