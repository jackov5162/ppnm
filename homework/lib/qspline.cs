using System;
using static System.Math;
public class qspline{
    public vector x, y, b, c;
    public int bin, n;
    public qspline(vector xs, vector ys){
        x = xs.copy();
        y = ys.copy();
        n = x.size;
        c = new vector(n - 1);
        b = new vector(n - 1);

        vector p = new vector(n - 1);
        vector h = new vector(n - 1);
        for(int i = 0; i < n - 1; i++){
            h[i] = x[i + 1] - x[i];
            p[i] = (y[i + 1] - y[i]) / h[i];
        }
        c[0] = 0;

        for(int i = 0; i < n - 2; i++){
            c[i + 1] = (p[i + 1] - p[i] - c[i] * h[i]) / h[i + 1];
        }
        c[n - 2] /= 2;

        for(int i = n - 3; i >=0; i--){
            c[i] = (p[i + 1] - p[i] - c[i + 1] * h[i + 1]) / h[i];
        }

        for(int i = 0; i < n - 1; i++){
            b[i] = p[i] - c[i] * h[i];
        } 
    }
    public void binsearch(double z){
        /* locates the interval for z by bisection */
        if(z < x[0] || z > x[n - 1]){
            throw new Exception("binsearch: bad z");
        }
        bin = 0;
        int j = n - 1;
        while(j - bin > 1){
            int mid = (bin + j) / 2;
            if(z > x[mid]){
                bin = mid;
            }
            else{
                j = mid;
            }
        }
    }
    public double evaluate(double z){
        binsearch(z);
        double h = z - x[bin];
        return y[bin] + h * b[bin] + Pow(h, 2) * c[bin];
    }
    public double derivative(double z){
        binsearch(z);
        double h = z - x[bin];
        return b[bin] + h * 2 * c[bin];
    }
    public double integral(double z){
        binsearch(z);
        double sum = 0;
        double h;
        for(int i =0; i < bin; i++){
            h = x[i + 1] - x[i];
            sum += h * y[i] + Pow(h, 2) / 2 * b[i] + Pow(h, 3) / 3 * c[i];
        }
        h = z - x[bin];
        sum += h * y[bin] + Pow(h, 2) / 2 * b[bin] + Pow(h, 3) / 3 * c[bin];
        return sum;
    }
}