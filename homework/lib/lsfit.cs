using System;
public static class lsfit{
    public static (vector, matrix) Lsfit(Func<double, double>[] fs, vector x, vector y, vector dy){
        matrix A = new matrix(x.size, fs.Length);
        vector b = new vector(x.size);
        for(int i = 0; i < x.size; i++){
            b[i] = y[i]/dy[i];
            for(int k = 0; k < fs.Length; k++){
                A[i, k] = fs[k](x[i])/dy[i];
            }
        }
        QRGS QR = new QRGS(A);
        vector c = QR.solve(b);
        matrix R = QR.R;
        matrix RI = R.inverse();
        matrix covariance = RI * RI.T;

        return (c, covariance);
    }
}