using System;
public class hydrogen{
    public static double Fe(double E, double r, double r_min = 0.0001){
        if(r < r_min) return r-r*r;

        Func<double, vector, vector> swaveradialS = (x, y) => {
            double f = y[0];
            double ddf = y[1];
            return new vector(ddf, 2 * (-1 / x - E) * f);
        };
        vector ystart = new vector(r_min - r_min * r_min, 1 - 2 * r_min);
        var interval = (r_min, r);
        (vector xlist, matrix ymatrix) = ODEDriver.driver(swaveradialS, interval, ystart, h:1e-2, acc:1e-3, eps:1e-3);
        vector ylist = ymatrix[xlist.size];
        return ylist[0];
    }
}