using System;
using static System.Math;
public static class RQAIntegrator{
    public static double integrate(
        Func<double,double> f, double a, double b,
        double theta=0.001, double epsilon=0.001, double f2=Double.NaN, double f3=Double.NaN,
        string VT = "None")
        {
        double start;
        double end;
        Func<double,double> func;
        if(VT == "CC"){
            func = (t => f((a + b) / 2 + (b - a) / 2 * Cos(t)) * Sin(t) * (b - a) / 2);
            start = 0;
            end = PI;
        }
        else{
            func = f;
            start = a;
            end = b;
        }
        double h = end - start;
        if(Double.IsNaN(f2)){
            f2 = f(start + 2 * h / 6);
            f3 = f(start + 4 * h / 6);
        }
        double f1 = func(start + h / 6);
        double f4 = func(start + 5 * h / 6);
        double Q = (2 * f1 + f2 + f3 + 2 * f4) / 6 * (end - start);
        double q = (f1 + f2 + f3 + f4) / 4 * (end - start);
        double err = Abs(Q - q);
        if(err <= theta + epsilon * Abs(Q)) {
            return Q;
        }
        else return integrate(func, start, (start + end) / 2, theta / Sqrt(2), epsilon, f1, f2) + integrate(func, (start + end) / 2, end, theta / Sqrt(2), epsilon, f3, f4);
    }
    public static double errorFunction(double z){
        if(z < 0){
            return -errorFunction(-z);
        }
        else{
            Func<double,double> f;
            if(1 < z){
                f = (t => Exp(-Pow(z + (1 - t) / t, 2)) / t / t);
                return 1 - 2 / Sqrt(PI) * integrate(f, 0, 1);
            }
            else{
                f = (t => Exp(-t*t));
                return 2 / Sqrt(PI) * integrate(f, 0, z);
            }
        }
    }
}