using System;
using static System.Console;
using static System.Math;

public class vector{
	private double[] data;
	
	public int size => data.Length;

	public vector(int n){
		data = new double[n];
	}
	public vector(params double[] list){
		data = list;
	}

	public static implicit operator vector (double[] a){
		return new vector(a);
	}
	public static implicit operator double[] (vector v){
		return v.data;
	}

	public double this[int i]{
		get => data[i];
		set => data[i] = value;
	}

	public static vector operator* (vector v, double x){
		var c = new vector(v.size);
		for(int i=0; i<v.size; i++){
			c[i] = x * v[i];
		}
		return c;
	}

	public static vector operator* (double x, vector v){
		return v * x;
	}

	public static vector operator/ (vector v, double x){
		var c = new vector(v.size);
		for(int i=0; i<v.size; i++){
			c[i] = v[i] / x;
		}
		return c;
	}

	public static vector operator/ (vector v, vector u){
		var w = new vector(v.size);
		for(int i=0; i<v.size; i++){
			w[i] = v[i] / u[i];
		}
		return w;
	}

	public static vector operator+ (vector v, vector u){
		var w = new vector(v.size);
		for(int i=0; i<v.size; i++){
			w[i] = v[i] + u[i];
		}
		return w;
	}

	public static vector operator+ (vector v, double x){
		var w = new vector(v.size);
		for(int i=0; i<v.size; i++){
			w[i] = v[i] + x;
		}
		return w;
	}

	public static vector operator- (vector v, vector u){
		var w = new vector(v.size);
		for(int i=0; i<v.size; i++){
			w[i] = v[i] - u[i];
		}
		return w;
	}

	public static vector operator- (vector v){
		var u = new vector(v.size);
		for(int i=0; i<v.size; i++){
			u[i] = -v[i];
		}
		return u;
	}

	public static vector operator* (vector v, vector u){
		var w = new vector(v.size);
		for(int i=0; i<v.size; i++){
			w[i] += v[i] * u[i];
		}
		return w;
	}

	public vector map(System.Func<double,double> f){
		vector v=new vector(size);
		for(int i=0;i<size;i++)v[i]=f(this[i]);
		return v;
	}

	public double maxabs(){
		double r=Abs(this[0]);
		for(int i=1;i<size;i++)if(Abs(this[i])>r)r=Abs(this[i]);
		return r;
	}

	public vector log(){
		vector c = this.copy();
		for(int i = 0; i < size; i++) c[i] = Log(c[i]);
		return c;
	}

	public vector absVec(){
		vector c = this.copy();
		vector c2 = new vector(size);
		for(int i = 0; i < this.size; i++){
			c2[i] = Abs(c[i]);
		}
		return c2;
	}

	public int max(){
		int index = 0;
		double r = this[0];
		for(int i=1;i<size;i++)if(this[i] > r){
			r = this[i];
			index = i;
		}
		return index;
	}

	public int min(){
		int index = 0;
		double r = this[0];
		for(int i=1;i<size;i++)if(this[i] < r){
			r = this[i];
			index = i;
		}
		return index;
	}

	public double norm(){
		double mean_abs = 0;
		for(int i=0; i<size; i++){
			mean_abs += Abs(this[i]);
		}
		if(mean_abs==0){
			mean_abs = 1;
		}
		mean_abs /= size;
		double sum = 0;
		for(int i=0; i<size; i++){
			sum += (this[i] / mean_abs) * (this[i] / mean_abs);
		}
		double norm = mean_abs * Sqrt(sum);
		return norm;
	}
	public double norm2(){
		double s = 0;
		vector c = this.copy();
		for(int i = 0; i < this.size; i++){
			s += Pow(c[i], 2);
		}
		s = Sqrt(s);
		return s;
	}

	public double dot(vector a){
		double dot = 0;
		for(int i=0; i<size; i++){
			dot += this[i] * a[i];
		}
		return dot;
	}
	
	public static double operator% (vector v, vector u){
		return v.dot(u);
	}

	public vector copy(){
		vector copy = new vector(size);
		for(int i=0; i<size; i++){
			copy[i] = this[i];
		}
		return copy;
	}

	public vector onlyk(int k){
		vector copy = new vector(size);
		for(int i=0; i<size; i++){
			copy[i] = 0;
		}
		copy[k] = this[k];
		return copy;
	}

	public vector onlyone(int k){
		vector copy = new vector(size);
		for(int i=0; i<size; i++){
			copy[i] = 0;
		}
		copy[k] = 1;
		return copy;
	}

	public vector add(double x){
		vector add = new vector(size + 1);
		for(int i=0; i<size; i++){
			add[i] = this[i];
		}
		add[size] = x;
		return add;
	}

	public void print(string name = "", string format="{0,7:g10} "){
		if(!String.IsNullOrEmpty(name)){
			Write($""+name+": ");
		}
		for(int i=0; i<this.size; i++){
			Write(format,this[i]);
		}
		Write("\n");
	}

	public static bool approx(double x, double y, double acc=1e-9, double eps=1e-9){
		if(Abs(x-y)<acc)return true;
		if(Abs(x-y)/Max(Abs(x),Abs(y))<eps)return true;
		return false;
	}

	public static bool approx(vector a,vector b,double acc=1e-9,double eps=1e-9){
		if(a.size!=b.size)return false;
		for(int i=0;i<a.size;i++)
			if(!approx(a[i],b[i],acc,eps))return false;
		return true;
	}
	public bool approx(vector o){
		for(int i=0;i<size;i++)
			if(!approx(this[i],o[i]))return false;
	return true;
	}
}
