using System;
using static System.Math;
public static class RK_ODE{
    public static (vector,vector) rkstep23(
        Func<double,vector,vector> f,
        double x,
        vector y,
        double h
    ){
        vector k0 = f(x, y);
        vector k1 = f(x+h/2, y+k0*h/2);
        vector k2 = f(x+h*3/4, y+3/4*k1*h);
        vector ka = 2.0/9*k0 + 3.0/9*k1 + 4.0/9*k2;
        vector yh = y+ka*h;
        vector dy = (ka-k1)*h;
        return (yh, dy);
    }
    public static (vector,vector) rkstep12(
        Func<double,vector,vector> f,
        double x,
        vector y,
        double h
    ){
        vector k0 = f(x, y);
        vector k1 = f(x+h/2, y+k0*h/2);
        vector yh = y+k1*h;
        vector dy = (k1-k0)*h;
        return (yh, dy);
    }
}