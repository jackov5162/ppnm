using System;
using static System.Math;
public class qspline_matrix_version{
    public vector x, dx;
    public matrix y, b, c, dy;
    public int bin;
    public qspline_matrix_version(vector xs, matrix ys){
        x = xs.copy();
        y = ys.copy();
        dx = new vector(x.size - 1);
        dy = new matrix(y.size1 - 1, y.size2);
        c = new matrix(y.size1 - 1, y.size2);
        b = new matrix(y.size1 - 1, y.size2);
        for(int i = 0; i < dx.size; i++){
            dx[i] = x[i + 1] - x[i];
            dy[i] = y[i + 1] - y[i];
        }
        for(int i = 0; i < y.size2; i++){
            c[0,i] = 0;
        }
        for(int i = 0; i < dx.size - 1; i++){
            c[i + 1] = ((dy[i + 1] / dx[i + 1] - dy[i] / dx[i]) - c[i] * dx[i]) / dx[i + 1];
        }
        c[dx.size - 1] /= 2;
        for(int i = dx.size - 2; i >= 0; i--){
            c[i] = ((dy[i + 1] / dx[i + 1] - dy[i] / dx[i]) - c[i + 1] * dx[i + 1]) / dx[i];
        }
        for(int i = 0; i < dx.size; i++){
            b[i] = dy[i] / dx[i] - c[i] * dx[i];
        }
    }
    public void binsearch(double z){
        /* locates the interval for z by bisection */
        if(z < x[0] || z > x[x.size - 1]){
            throw new Exception("binsearch: bad z");
        }
        bin = 0;
        int j = x.size - 1;
        while(j - bin > 1){
            int mid = (bin + j) / 2;
            if(z > x[mid]){
                bin = mid;
            }
            else{
                j = mid;
            }
        }
    }
    public Func<double,vector> evaluate(){
        Func<double,vector> interpolant = delegate(double z){
            binsearch(z);
            double h = z - x[bin];
            return y[bin] + h * b[bin] + h * c[bin];
        };
        return interpolant;
    }
}