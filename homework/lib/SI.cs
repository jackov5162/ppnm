using System;
using static System.Math;
public class SI{
    public vector x, y, dx, dy;
    public int bin;
    double h;
    public SI(vector xs, vector ys){
        x = xs.copy();
        y = ys.copy();
        dx = new vector(x.size - 1);
        dy = new vector(x.size - 1);
        for(int i = 0; i < dx.size; i++){
            dx[i] = x[i + 1] - x[i];
            dy[i] = y[i + 1] - y[i];
        }
    }

    public double linterp(double z){
        binsearch(z);
        double h = z - x[bin];
        return y[bin] + dy[bin] / dx[bin] * h;
    }

    public double linterpInteg(double z){
        binsearch(z);
        double sum = 0;
        for(int i =0; i < bin; i++){
            h = x[i + 1] - x[i];
            sum += y[i] * h + dy[i] / dx[i] * Pow(h, 2) / 2;
        }
        h = z - x[bin];
        sum += y[bin] * h + dy[bin] / dx[bin] * Pow(h, 2) / 2;
        return sum;
    }

    public void binsearch(double z){
        /* locates the interval for z by bisection */
        if(z < x[0] || z > x[x.size - 1]){
            throw new Exception("binsearch: bad z");
        }
        bin = 0;
        int j = x.size - 1;
        while(j - bin > 1){
            int mid = (bin + j) / 2;
            if(z > x[mid]){
                bin = mid;
            }
            else{
                j = mid;
            }
        }
    }
}