using System;
using static System.Math;

public class QRGS{
	
	public matrix Q, R;

	public QRGS(matrix A){
		int size2 = A.size2;
		Q = A.copy();
		R = new matrix(size2,size2);
		for(int i = 0; i < size2; i++){
			R[i,i] = Q[i].norm();
			Q[i] /= R[i,i];
			for(int j = i + 1; j < size2; j++){
				R[i,j] = Q[i].dot(Q[j]);
				Q[j] -= Q[i] * R[i,j];
			}
		}
		
	}

	public vector solve(vector b){
		vector c = Q.T * b;
		for(int i = c.size - 1; i >= 0; i--){
			double sum=0;
			for(int k = i + 1; k < c.size; k++){
				sum += R[i,k] * c[k];
			}
			c[i] = (c[i] - sum) / (R[i,i]);
		}
		return c;
	}
	
	public double det(){
		double sum=1;
		for(int i = 0; i < R.size1; i++){
			sum *= R[i,i];
		}
		return sum;
	}
	public matrix inverse(){
		matrix inverse_A = new matrix(Q.size1,Q.size2);
		vector e = new vector(Q.size1);
		for(int i = 0; i < Q.size2; i++){
			e = e.onlyone(i);
			inverse_A[i] = solve(e);
		}
		return inverse_A;
	}
}
