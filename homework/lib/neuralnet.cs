using System;
using static System.Math;
using static System.Random;

public class ann{
    int n; /* number of hidden neurons */
    Func<double,double> f = x => x*Exp(-x*x); /* activation function */
    public vector initial_p, p; /* network parameters */
    public ann(int n){
        this.n = n;
        }
    void createp(){
        this.p = new vector(3 * this.n);
        var rnd = new Random();
        for(int i = 0; i < 3 * this.n; i++){
            this.p[i] = rnd.NextDouble();
        }
        this.initial_p = this.p.copy();

    }
    public double response(double x, vector pp){
        /* return the response of the network to the input signal x */
        double sum = 0;
        for(int i = 0; i < n; i++){
            double a = pp[3 * i];
            double b = pp[3 * i + 1];
            double w = pp[3 * i + 2];
            sum += f((x - a) / b) * w;
        }
        return sum;

    }
    public void train(vector x,vector y, vector p){
        this.p = p;
        trainpt2(x,y);
    }
    public void train(vector x,vector y){
        createp();
        trainpt2(x,y);
    }
    public void trainpt2(vector x,vector y){
        /* train the network to interpolate the given table {x,y} */
        int size = x.size;
        Func<vector,double> C = p => {
            double sum = 0;
            for(int i = 0; i < size; i++){
                sum += Pow((response(x[i], p) - y[i]), 2);
            }
            return sum;
        };
        minimisation mini = new minimisation(C);
        p = mini.newton(p, 1e-4);
        bool isnan = mini.nanresult();
        /* If the change is too big or small the result is nan. 
        This restarts it with new parameter guesses in p. */
        if(isnan){ 
            train(x,y);
        }
    }
}