Test with Quadratic function
f(x,y) = (x - 25)^2 + (y^2 - x)^2
_______________
iterations: 5
Initial guess:       0       1 
Final guess  : 24.99999933 4.999999903 

The roots gives: f =>   0.000
Type of extrema: minimum

_______________
iterations: 0
Initial guess:      25       5 
Final guess  :      25       5 

The roots gives: f =>   0.000
Type of extrema: minimum

Test with Rosenbrock's valley function
_______________
iterations: 0
Initial guess:       1       1 
Final guess  :       1       1 

The roots gives: f =>   0.000
Type of extrema: minimum

_______________
iterations: 1
Initial guess: 0.99999 0.99999 
Final guess  : 0.9999951585 0.9999903096 

The roots gives: f =>   0.000
Type of extrema: minimum

Test with Himmelblau's function
_______________
iterations: 0
Initial guess:       3       2 
Final guess  :       3       2 

The roots gives: f =>   0.000
Type of extrema: minimum

_______________
iterations: 5
Initial guess:     2.5     1.5 
Final guess  : 2.999999948 2.000000091 

The roots gives: f =>   0.000
Type of extrema: minimum

Higgs boson:
_______________
iterations: 6599
Initial guess:   125.3      10 
Final guess  : 117.149716 76.52685972 

The roots gives: f =>  45.509