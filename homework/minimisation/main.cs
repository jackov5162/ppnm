using System;
using static System.Math;
using static System.Console;
using System.Collections.Generic;

public static class main{
    public static void Main(){
        double ε2 = Pow(2, -26);
        double limit = 1e-3;
        WriteLine($"Test with Quadratic function");
        WriteLine($"f(x,y) = (x - 25)^2 + (y^2 - x)^2");
        double[,] quad_doublematrix = {{0, 25},
                                       {1, 5}};
        matrix quad_matrix = new matrix(quad_doublematrix);
        Func<vector, double> quad_f = (t) => {
            double y = Pow(t[0] - 25, 2) + Pow(t[1] * t[1] - t[0], 2);
            return y;
        };

        for(int i = 0; i < quad_matrix.size1; i++){
            vector quad_x = quad_matrix[i];
            minimisation quad = new minimisation(quad_f, ε2);
            vector quad_answer = quad.newton(quad_x, limit);
            double quad_func = quad_f(quad_answer);
            string quad_extrema = quad.extrema();
            WriteLine($"_______________");
            WriteLine($"iterations: {quad.nsteps}");
            quad_x.print("Initial guess");
            quad_answer.print("Final guess  ");
            Write($"\nThe roots gives: f => ");
            Write("{0,7:f3}",quad_func);
            WriteLine();
            WriteLine($"Type of extrema: {quad_extrema}");
            WriteLine();
        }
        WriteLine($"Test with Rosenbrock's valley function");
        double[,] RB_doublematrix = {{1, 0.99999},
                                     {1, 0.99999}};
        matrix RB_matrix = new matrix(RB_doublematrix);
        Func<vector, double> RB_f = (t) => {
            double y = Pow(1 - t[0], 2) + 100 * Pow(t[1] - t[0] * t[0], 2);
            return y;
        };

        for(int i = 0; i < RB_matrix.size1; i++){
            vector RB_x = RB_matrix[i];
            minimisation RB = new minimisation(RB_f, ε2);
            vector RB_answer = RB.newton(RB_x, limit);
            double RB_func = RB_f(RB_answer);
            string RB_extrema = RB.extrema();
            WriteLine($"_______________");
            WriteLine($"iterations: {RB.nsteps}");
            RB_x.print("Initial guess");
            RB_answer.print("Final guess  ");
            Write($"\nThe roots gives: f => ");
            Write("{0,7:f3}",RB_func);
            WriteLine();
            WriteLine($"Type of extrema: {RB_extrema}");
            WriteLine();
        }
        
        WriteLine($"Test with Himmelblau's function");
        double[,] HB_doublematrix = {{3.0, 2.5, 3.4},
                                     {2.0, 1.5, 3.2}};
        matrix HB_matrix = new matrix(HB_doublematrix);
        Func<vector, double> HB_f = (t) => {
            double y = Pow(t[0] * t[0] + t[1] - 11, 2) + Pow(t[0] + t[1] * t[1] - 7, 2);
            return y;
        };
        for(int i = 0; i < HB_matrix.size1; i++){
            vector HB_x = HB_matrix[i];
            minimisation HB = new minimisation(HB_f, ε2);
            vector HB_answer = HB.newton(HB_x);
            double HB_func = HB_f(HB_answer);
            string HB_extrema = HB.extrema();	    
            WriteLine($"_______________");
            WriteLine($"iterations: {HB.nsteps}");
            HB_x.print("Initial guess");
            HB_answer.print("Final guess  ");
            Write($"\nThe roots gives: f => ");
            Write("{0,7:f3}",HB_func);
            WriteLine();
            WriteLine($"Type of extrema: {HB_extrema}");
            WriteLine();
        }
        var energy = new List<double>();
        var signal = new List<double>();
        var error = new List<double>();
        var separators = new char[] {' ','\t'};
        var options = StringSplitOptions.RemoveEmptyEntries;
        do{
            string line=Console.In.ReadLine();
            if(line==null)break;
            string[] words=line.Split(separators,options);
            energy.Add(double.Parse(words[0]));
            signal.Add(double.Parse(words[1]));
            error .Add(double.Parse(words[2]));
        }while(true);
        
        vector higgs_x = new vector(125.3, 10);
        Func<double, vector, double> higgs_F = (Energy, v) => {
            double E    = Energy;
            double mass = v[0];
            double Γ    = v[1];
            //double A = 0.188;
            double y = 1 / (Pow(E - mass, 2) + Pow(Γ, 2) / 4);
            return y;
        };

        Func<vector, double> higgs_D = (t) => {
            double y = 0;
            for(int i = 0; i < energy.Count; i++) y += Pow((higgs_F(energy[i], t) - signal[i])/ error[i], 2);
            return y;
        };
        minimisation higgs = new minimisation(higgs_D, ε2);
        vector higgs_answer = higgs.newton(higgs_x);
        double higgs_func = higgs_D(higgs_answer);
        WriteLine("Higgs boson:");
        WriteLine($"_______________");
        WriteLine($"iterations: {higgs.nsteps}");
        higgs_x.print("Initial guess");
        higgs_answer.print("Final guess  ");
        Write($"\nThe roots gives: f => ");
        Write("{0,7:f3}",higgs_func);


    }
}
