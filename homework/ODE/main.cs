using System;
using static System.Console;
using static System.Math;
public class main{
    public static void Main(){
        Func<double, vector, vector> fs = (t, y) => {
            double theta = y[0];
            double omega = y[1];
            double b = 0.25;
            double c = 5.0;
            double dydt_theta = omega;
            double dydt_omega = -b * omega - c * Math.Sin(theta);
            return new vector(dydt_theta, dydt_omega);
        };
        var interval = (0,10);
        vector ystart = new vector(Math.PI - 0.1, 0);
        (vector xlist,matrix ylist) = ODEDriver.driver(fs, interval, ystart);
        double length = xlist.size;
        for(int i = 0; i < length; i++){
            Write($"{xlist[i]} ");
            ylist[i].print();
        }
        Write($"\n");
        Write($"\n");
        Write($"\n");
        Write($"Alternative interface:\n");
        Write($"I: \n");
        Func<double, vector, vector> fs_I = (t, y) => {
            double theta = y[0];
            double omega = y[1];
            double dydt_theta = omega;
            double dydt_omega = 1 - theta;
            return new vector(dydt_theta, dydt_omega);
        };
        vector ystart_I = new vector(1, 0);
        (vector xlist_I,matrix ylist_I) = ODEDriver.driver(fs_I, interval, ystart_I);
        double length_I = xlist_I.size;
        for(int i = 0; i < length_I; i++){
            Write($"{xlist_I[i]} ");
            ylist_I[i].print();
        }        
        Write($"\n");
        Write($"\n");
        Write($"\n");
        Write($"II: \n");
        vector ystart_II = new vector(1, -0.5);
        (vector xlist_II,matrix ylist_II) = ODEDriver.driver(fs_I, interval, ystart_II);
        double length_II = xlist_II.size;
        for(int i = 0; i < length_II; i++){
            Write($"{xlist_II[i]} ");
            ylist_II[i].print();
        }        
        Write($"\n");
        Write($"\n");
        Write($"\n");
        Write($"III: \n");
        Func<double, vector, vector> fs_III = (t, y) => {
            double theta = y[0];
            double omega = y[1];
            double dydt_theta = omega;
            double dydt_omega = 1 - theta + 0.01 * theta * theta;
            return new vector(dydt_theta, dydt_omega);
        };
        vector ystart_III = new vector(1, -0.5);
        (vector xlist_III,matrix ylist_III) = ODEDriver.driver(fs_III, interval, ystart_III);
        double length_III = xlist_III.size;
        for(int i = 0; i < length_III; i++){
            Write($"{xlist_III[i]} ");
            ylist_III[i].print();
        }        
    }
}