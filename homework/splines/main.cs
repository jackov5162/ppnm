using System;
using static System.Console;
using static System.Math;
using System.IO;
using System.Text;

public static class main{
    public static void Main(){
        int n = 8;
        int N = 200;
        int i;
        vector x = new vector(n);
        vector cos = new vector(n);
        vector sin = new vector(n);

        for(i = 0; i < n; i++){
            x[i] = 3 * PI * i / (n - 1);
            cos[i] = Cos(x[i]);
            sin[i] = Sin(x[i]);
        }
        using (StreamWriter data = new StreamWriter("data.txt", false))
        {
            for(i = 0; i < x.size; i++){
                data.Write($"{x[i]} {cos[i]} {sin[i]}");
                if(i < x.size - 1){
                    data.Write($"\n");
                }
            }
        }
        SI LSI = new SI(x, cos);    
        qspline QSI = new qspline(x, sin);
        cspline CSI = new cspline(x, sin);

        vector Qb = QSI.b;
        vector Qc = QSI.c;

        vector Cb = CSI.b;
        vector Cc = CSI.c;
        vector Cd = CSI.d;

        Write($"____________________\n");
        Write($"Quadratic spline\n");
        Write($"b: {Qb} c: {Qc}\n");
        Write($"____________________\n");
        Write($"Cubic spline\n");
        Write($"b: {Cb} c: {Cc} d: {Cd}\n");
        Write($"\n");
        Write($"\n");
        Write($"\n");
        Write($"x cos(x) sin(x) 1-cos(x) LS(x) LSint(x) QS(x) QSder(x) QSint(x) CS(x) CSder(x) CSint(x)\n");
        double z, step = (x[n - 1] - x[0]) / (N - 1);
        for(z = x[0], i = 0; i < N; z = x[0]+(++i) * step){
            WriteLine($"{z} {Cos(z)} {Sin(z)} {1 - Cos(z)} {LSI.linterp(z)} {LSI.linterpInteg(z)} {QSI.evaluate(z)} {QSI.derivative(z)} {QSI.integral(z)} {CSI.evaluate(z)} {CSI.derivative(z)} {CSI.integral(z)}");
        }

   }
}