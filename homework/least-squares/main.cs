using System;
using static System.Console;
using static System.Math;
using System.IO;
public static class main {
    public static void Main(){
        string textfilePath = @"input.txt";
		string[] data_ = File.ReadAllLines(textfilePath);
        matrix data = new matrix(data_);
        vector x = new vector(data[0]);
        vector y = new vector(data[1].log());
        vector dy = new vector(data[2]/data[1]);
        var fs = new Func<double, double>[] {t => 1.0, t => -t};
        (vector solution, matrix covariance) = lsfit.Lsfit(fs, x, y, dy);
        for(int i = 0; i < solution.size; i++){
            WriteLine($"{solution[i]}");
            WriteLine($"{covariance[i, i]}");
        }
        using (StreamWriter HT = new StreamWriter("Half-time.txt", false))
        {
            double uncertainty = Abs(Log(2)/solution[1]*covariance[1, 1]/solution[1]);
            HT.WriteLine($"the half-time is {Log(2)/solution[1]} +- {uncertainty}");
        }
    }
}