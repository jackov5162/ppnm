using System;
using static System.Console;
using static System.Math;
public class main{
    public static void Main(){
        double start = 0;
        double end = 1;
        Func<double,double> F_I = (t => Sqrt(t));
        Func<double,double> F_II = (t => 1 / Sqrt(t));
        Func<double,double> F_III = (t => 4*Sqrt(1 - Pow(t, 2)));
        Func<double,double> F_IV = (t => Log(t) / Sqrt(t));
        double I = RQAIntegrator.integrate(F_I, start, end);
        double II = RQAIntegrator.integrate(F_II, start, end);
        double III = RQAIntegrator.integrate(F_III, start, end);
        double IV = RQAIntegrator.integrate(F_IV, start, end);
        double CC_I = RQAIntegrator.integrate(F_II, start, end, VT:"CC");
        double CC_II = RQAIntegrator.integrate(F_IV, start, end, VT:"CC");
        WriteLine($"Accuracy goal: 0.001");
        WriteLine($"RAI Answers");
        WriteLine($"First integral is 2/3. Integrator gives {I}");
        WriteLine($"Second integral is 2. Integrator gives {II}");
        WriteLine($"Third integral is pi. Integrator gives {III}");
        WriteLine($"Fourth integral is -4. Integrator gives {IV}");
        WriteLine();
        WriteLine($"CC Answers");
        WriteLine($"First integral is 2. CC integrator gives {CC_I}");
        WriteLine($"Second integral is -4. CC integrator gives {CC_II}");
        WriteLine("");
        WriteLine("");
        WriteLine("");
        WriteLine("Error Function");
        WriteLine("x, erf(x)");
        for(double x = -10; x < 10; x+=0.1){
            WriteLine($"{x} {RQAIntegrator.errorFunction(x)}");
        }
    }
}