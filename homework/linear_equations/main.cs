using System;
using static System.Console;
using System.IO;
using System.Text;
public static class main{
	public static void Main(){
		WriteLine($"\n_______________________________________________");
		WriteLine($"Testing initial QRGS on random matrix A where n > m:\n");
		int size1 = 6;
		int size2 = 4;
		matrix A = random_matrix(size1,size2);
		QRGS qrgs = new QRGS(A);
		matrix Q = qrgs.Q;
		matrix R = qrgs.R;
		double D = qrgs.det();
		matrix QT = Q.T;
		matrix QTQ = QT * Q;
		matrix QR = Q * R;
		matrix I = QTQ.I;
		WriteLine($"\n");
		A.print("A");
		WriteLine($"\n");
		Q.print("Q");
		WriteLine($"\n");
		QT.print("Q^T");
		WriteLine($"\n");
		R.print("R");
		WriteLine($"\n");
		WriteLine($"(R can be seen to be a upper triangular matrix)\n");
		WriteLine($"\n");
		WriteLine($"Determinant: {D}");
		WriteLine($"\n");
		WriteLine($"Q*R is equal to A: {QR.approx(A)}\n");
		WriteLine($"\n");
		WriteLine($"Q^T*Q is a identity matrix: {QTQ.approx(I)}\n");
		WriteLine($"\n_______________________________________________");
		WriteLine($"Testing QRGS solve on random square matrix A:\n");
		int size1_ = 5;
		int size2_ = 5;
		matrix A_ = random_matrix(size1_,size2_);
		QRGS qrgs_ = new QRGS(A_);
		vector b_ = random_vector(size2_);
		matrix Q_ = qrgs_.Q;
		matrix R_ = qrgs_.R;
		double D_ = qrgs_.det();
		vector x_ = qrgs_.solve(b_);
		vector Ax_ = A_ * x_;
		matrix B_ = qrgs_.inverse();
		matrix I_ = A_.I;
		matrix AB_ = A_ * B_;
		WriteLine($"\n");
		A_.print("A");
		WriteLine($"\n");
		B_.print("Inverse A");
		WriteLine($"\n");
		AB_.print("A*Inverse A");
		WriteLine($"\n");
		Q_.print("Q");
		WriteLine($"\n");
		R_.print("R");
		WriteLine($"(R can be seen to be a upper triangular matrix)\n");
		b_.print("b");
		Write($"\n");
		WriteLine($"solving for x in QRx = b using the form Rx = bQ^T. \n");
		x_.print("x");
		WriteLine($"\n");
		WriteLine($"Determinant: {D_}");
		WriteLine($"\n");
		WriteLine($"A*x is equal to b: {Ax_.approx(b_)}\n");
		
		WriteLine($"A*inverse A is a identity matrix: {AB_.approx(I_)}\n");
		WriteLine($"\n_______________________________________________");
	}
	public static matrix random_matrix(int n, int m){
		matrix mat = new matrix(n, m);
		var rnd = new System.Random(1);
		for(int i=0; i<n; i++){
			for(int j=0; j<m; j++){
				mat[i, j] = rnd.NextDouble();
			}
		}
		return mat;
	}
	public static vector random_vector(int n){
		vector vec = new vector(n);
		var rnd = new System.Random(2);
		for(int i=0; i<n; i++){
			vec[i] = rnd.NextDouble();
		}
		return vec;
	}
}