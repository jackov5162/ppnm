using System;
using System.Linq;
using static System.Console;
using System.Threading;
using System.Diagnostics;
class main{
class data{ public int a, b; public double sum;}
    public static void harm(object obj){
        data arg = (data)obj;
        string name=Thread.CurrentThread.Name;
    	WriteLine($"Thread {name}: a={arg.a} b={arg.b}");
        arg.sum=0;
        for(int i = arg.a; i < arg.b; i++){
            arg.sum += 1.0/i;
        }
        WriteLine($"Thread {name} sumab = {arg.sum}");
    }
    public static int Main(string[] args){
        WriteLine();
        WriteLine();
        int nthreads = 1, nterms = (int)1e8;
        foreach(var arg in args){
            var words = arg.Split(':');
            if(words[0] == "-threads") nthreads = (int)float.Parse(words[1]);
            if(words[0] == "-terms"  ) nterms   = (int)float.Parse(words[1]);
        }
        WriteLine($"nterms={nterms} nthreads={nthreads}");
        Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();
        data[] pars = new data[nthreads];
        for(int i=0;i<nthreads;i++) {
            pars[i] = new data();
            pars[i].a = 1 + nterms/nthreads*i;
            pars[i].b = 1 + nterms/nthreads*(i+1);
        }
        pars[pars.Length-1].b=nterms+1; /* the enpoint might need adjustment */
        var threads = new System.Threading.Thread[nthreads];
        WriteLine("Main: now waiting for other threads...");
        for(int i=0;i<nthreads;i++) {
            threads[i] = new System.Threading.Thread(harm); /* create a thread */
            threads[i].Name = $"thread #{i+1}";
            threads[i].Start(pars[i]); /* run it with params[i] as argument to "harm" */
        }
        foreach(var thread in threads) thread.Join();
        double total=0; 
        foreach(var p in pars) total+=p.sum;
        stopwatch.Stop();
        TimeSpan ts = stopwatch.Elapsed;
        string elapsedTime = String.Format("{0:00}:{1:00}",
            ts.Seconds,
            ts.Milliseconds / 10);
        WriteLine();
        WriteLine($"Main: total sum = {total}");
        WriteLine("RunTime: " + elapsedTime);
        WriteLine();
        stopwatch.Restart();

        double parallel_sum=0;
        stopwatch.Start();
        System.Threading.Tasks.Parallel.For( 1, nterms+1, (int i) => parallel_sum+=1.0/i );
        stopwatch.Stop();
        ts = stopwatch.Elapsed;
        elapsedTime = String.Format("{0:00}:{1:00}",
            ts.Seconds,
            ts.Milliseconds / 10);
        Write($"Main: sum using parallel threads: {parallel_sum}\n");
        WriteLine("Parallel with double RunTime: " + elapsedTime);
        WriteLine();
        stopwatch.Restart();

        var Linq_sum = new System.Threading.ThreadLocal<double>( ()=>0, trackAllValues:true);
        stopwatch.Start();
        System.Threading.Tasks.Parallel.For( 1, nterms+1, (int i)=>Linq_sum.Value+=1.0/i );
        double Linq_totalsum=Linq_sum.Values.Sum();
        stopwatch.Stop();
        ts = stopwatch.Elapsed;
        elapsedTime = String.Format("{0:00}:{1:00}",
            ts.Seconds,
            ts.Milliseconds / 10);
        Write($"Main: sum using parallel threads: {Linq_totalsum}\n");
        WriteLine("Parallel with local thread RunTime: " + elapsedTime);
        WriteLine();
        stopwatch.Restart();
        return 0;
    }
}