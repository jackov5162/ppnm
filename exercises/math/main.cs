using System;
using static System.Console;
using static System.Math;
public class main{
	static int Main(){
		double sqrt2 = Sqrt(2.0);
		Write($"sqrt2^2 = {sqrt2*sqrt2} (should equal 2)\n");
		Write("\n");
		double prod = 1;
		for(double x=1; x<10; x++){
			Write($"for fgamma{x}={sfuns.fgamma(x)}. \t {x-1}!={prod}\n");
			prod *= x;
		}
		Write("\n");
		prod = 1;
		for(double x=1; x<10; x++){
			Write($"for log(fgamma{x})={sfuns.lngamma(x)}. \t {x-1}!={Log(prod)}\n");
			prod *= x;
		}
		return 0;
	}
}
