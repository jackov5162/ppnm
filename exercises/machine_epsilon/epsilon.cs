using System;
using static System.Math;
using static System.Console;
static class main{
	static double epsilon=Pow(2,-52);
	static double tiny=epsilon/2;
	static double a=1+tiny+tiny;
	static double b=tiny+tiny+1;
	static int Main(){
		int i=1;
		double x=1;
		float y=1F;
		Write("Machine Epsilon:\n");
		while(i+1>i) {i++;};
		Write("my max int = {0}\n",i);
		while(i-1<i) {i++;};
		Write("my min int = {0}\n",i);
		while(1+x!=1){x/=2;} 
		x*=2;
		while((float)(1F+y) != 1F){y/=2F;} 
		y*=2F;
		Write("my double machine epsilon = {0}\n",x);
		Write("my float machine epsilon = {0}\n",y);
		Write($"tiny = 2^(-52) / 2 = {tiny}\n");
		Write($"a = 1 + tiny + tiny = {a}\n");
		Write($"b = tiny + tiny + 1 = {b}\n");
		Write($"a==b ? {a==b}\n");
		Write($"a>1  ? {a>1}\n");
		Write($"b>1  ? {b>1}\n");
		Write("\n");
	return 0;
	}//Main
}//class main
