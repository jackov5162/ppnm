using System;
using static System.Math;
using static System.Console;
static class main{
	static double d1 = 0.1+0.1+0.1+0.1+0.1+0.1+0.1+0.1;
	static double d2 = 8*0.1;
	static int Main(){
		WriteLine("Comparing Doubles:");
		WriteLine($"d1 = {d1:e15}");
		WriteLine($"d2 = {d2:e15}");
		WriteLine($"d1==d2 ? => {d1==d2}");
		WriteLine($"With an approximation of 1e-9. The values are equal: {approx(d1,d2)}");
		WriteLine("\n");
		return 0;
	}//Main
	public static bool approx(double a, double b, double acc=1e-9, double eps=1e-9){
		double A = Abs(a-b);
		double B = Max(Abs(a),Abs(b));
		if(A <= acc)
			return true;
		if(A/B <= eps)
			return true;
		return false;
	}//Approx
}//class main
