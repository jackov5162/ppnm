Testing: sqrt(-1) = i^2
 passed.
Testing: sqrt(i) = 1 / sqrt(2) + I / sqrt(2)
 passed.
Testing: exp(i) = cos(1) + I * sin(1)
 passed.
Testing: exp(iπ) = cos(π) + I sin(π)
 passed.
Testing: i^(i) = exp(I * log(I))
 passed.
Testing: ln(i) = I * π / 2
 passed.
Testing: sin(iπ) = i * exp(π) / 2 - i * exp(-π) / 2
 passed.
