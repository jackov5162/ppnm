using System;
using static System.Math;
using static System.Console;
using static cmath;
public class main{
    public static void Main(){
        complex i = I;
        bool test = true;
        Write($"Testing: sqrt(-1) = i^2\n");
        complex first = i * i;
        test = first.approx(-1);
        if(test)Write(" passed.\n");
        else{Write(" FAILED.\n");}

        test = true;
        Write($"Testing: sqrt(i) = 1 / sqrt(2) + I / sqrt(2)\n");
        complex second = sqrt(i);
        test = second.approx(1 / sqrt(2) + i / sqrt(2));
        if(test)Write(" passed.\n");
        else{Write(" FAILED.\n");}

        test = true;
        Write($"Testing: exp(i) = cos(1) + I * sin(1)\n");
        complex third = exp(i);
        test = third.approx(Cos(1) + i * Sin(1));
        if(test)Write(" passed.\n");
        else{Write(" FAILED.\n");}

        test = true;
        Write($"Testing: exp(iπ) = cos(π) + I sin(π)\n");
        complex fourth = exp(i*PI);
        test = fourth.approx(Cos(PI) + i * Sin(PI));
        if(test)Write(" passed.\n");
        else{Write(" FAILED.\n");}

        test = true;
        Write($"Testing: i^(i) = exp(I * log(I))\n");
        complex fifth = i.pow(i);
        test = fifth.approx(exp(i * log(i)));
        if(test)Write(" passed.\n");
        else{Write(" FAILED.\n");}

        test = true;
        Write($"Testing: ln(i) = I * π / 2\n");
        complex sixth = log(i);
        test = sixth.approx(i * PI / 2);
        if(test)Write(" passed.\n");
        else{Write(" FAILED.\n");}

        test = true;
        Write($"Testing: sin(iπ) = i * exp(π) / 2 - i * exp(-π) / 2\n");
        complex seventh = sin(i * PI);
        test = seventh.approx(i * exp(PI) / 2 - i * exp(-PI) / 2);
        if(test)Write(" passed.\n");
        else{Write(" FAILED.\n");}
    }
}