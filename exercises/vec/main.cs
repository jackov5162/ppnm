using System;
using static System.Console;
using static System.Math;
class main{
static int Main(){
	int return_code = 0;
	bool test;
	var rnd = new Random();
	vec first_vector = new vec(Math.Round(rnd.NextDouble(), 3), Math.Round(rnd.NextDouble(), 3), Math.Round(rnd.NextDouble(), 3));
	vec second_vector = new vec(Math.Round(rnd.NextDouble(), 3), Math.Round(rnd.NextDouble(), 3), Math.Round(rnd.NextDouble(), 3));
	Write($"The vectors are\nv = ({first_vector})\nu = ({second_vector})\n");
	Write("\n");
	Write("Vector addition:\n");
	Write($"Answer: {first_vector + second_vector}.\n");
	Write("\n");
	Write("Vector subtraction:\n");
	Write($"Answer: {first_vector - second_vector}.\n");
	Write("\n");
	Write("Testing dot-product:\n");
	Write($"Answer: {first_vector.dot(second_vector)}.\n");
	test = first_vector.dot(second_vector) == vec.dot(first_vector, second_vector);
	Write($"v.dot(u) == vec.dot(v,u): {test}.\n");
	Write("\n");
	Write("Testing vector-product:\n");
	Write($"Answer: {first_vector.vector(second_vector)}.\n");
	test = vec.approx(first_vector.vector(second_vector), vec.vector(first_vector, second_vector));
	Write($"v.vector(u) == vec.vector(v,u): {test}.\n");
	Write("\n");
	Write("Testing norm of vector:\n");
	Write($"Answer: {first_vector.norm()}.\n");
	test = first_vector.norm() == vec.norm(first_vector);
	Write($"v.norm() == vec.norm(v): {test}.\n");
	return return_code;
	}//Main
}//main
