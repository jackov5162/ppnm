The vectors are
v = (0.075 0.466 0.188)
u = (0.38 0.183 0.527)

Vector addition:
Answer: 0.455 0.649 0.715.

Vector subtraction:
Answer: -0.305 0.283 -0.339.

Testing dot-product:
Answer: 0.212854.
v.dot(u) == vec.dot(v,u): True.

Testing vector-product:
Answer: 0.211178 0.031915 -0.163355.
v.vector(u) == vec.vector(v,u): True.

Testing norm of vector:
Answer: 0.508060035822539.
v.norm() == vec.norm(v): True.
