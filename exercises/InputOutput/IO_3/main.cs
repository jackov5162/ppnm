using System;
using static System.Math;
using System.IO;
public class main{
	public static int Main(string[] args){ /* Main function. Which take a list of strings as an input */
		string infile=null, outfile=null;
		foreach(var arg in args){ /* For each string... */
			var words = arg.Split(':'); /* split the string into itemtype and items */
			if(words[0]=="-input")infile=words[1];
			if(words[0]=="-output")outfile=words[1];
		}
		if( infile==null || outfile==null) {
			Console.Error.WriteLine("wrong filename argument");
			return 1;
		}
		var instream = new System.IO.StreamReader(infile);
		var outstream = new System.IO.StreamWriter(outfile, append:false);
		for(string line=instream.ReadLine(); line!=null; line=instream.ReadLine()){
			double x = double.Parse(line); /* convert numbertype to double */
			outstream.WriteLine($"{x} {Sin(x)} {Cos(x)}"); /* write the sinus and cosine */
		}
		instream.Close();
		outstream.Close();
		return 0;
	}
}
