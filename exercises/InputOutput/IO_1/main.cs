using System;
using static System.Math;

public class main{
	public static void Main(string[] args){ /* Main function. Which take a list of strings as an input */
		foreach(var arg in args){ /* For each string... */
			var words = arg.Split(':'); /* split the string into itemtype and items */
			if(words[0]=="-numbers"){ /* for the case of "numbers" */
				var numbers=words[1].Split(','); /* split up the numbers */
				foreach(var number in numbers){ /* for each number... */
					double x = double.Parse(number); /* convert numbertype to double */
					Console.Out.WriteLine($"{x} {Sin(x)} {Cos(x)}"); /* write the sinus and cosine */
				}
			}
		}
	}
}
