public class genlist<T>{ /* "T": type parameter */
	public T[] data; /* "data": array of items of the type "T" */
	public int size => data.Length; /* size of data array */
	public T this[int i] => data[i]; /* get i'th item in array using data[i] */
	public genlist(){ data = new T[0]; } /* readies an empty list */

	public void add(T item){ /* add item of the type "T" to the list */
		T[] newdata = new T[size+1]; /* Create a new bigger array to make room for new item */
		System.Array.Copy(data,newdata,size);  /* O(size) */
		newdata[size]=item;
		data=newdata;
	}

	public static implicit operator T[](genlist<T> list){
		return list.data;
	}
	public static implicit operator genlist<T>(T[] array){
		genlist<T> list=new genlist<T>();
		list.data=array;
		return list;
	}
}
