# Akima sub-spline interpolation (ASSI)
## Description:
Splines are a piecewise interpolation, which unlike the polynomial interpolation do not produce oscillations at the edges. Though they still produce them at discontinuities. These oscillations, called "wiggles", come partly from the periodic placement of the points used. As stabile oscillations with this period will still hit all the points. The Akima sub-spline interpolation (ASSI) solves this problem. Instead of getting maximum differentiability, like in cubic sub-spline interpolation, it disperses around substantial changes in the second derivative. Leading to a substantial decrease in wiggles around full discontinuities.
## a - implement Akima Sub-Spline interpolation:
I used the “splines“ assignment and the course book for inspiration for the implementation. We have the piecewise cubic polynomial: 
$$S(x)|_{x\in[x_i, x_{i+1}]} = a_i + b_i (x - x_i) + c_i (x - x)^2 + d_i (x - x_i)^3.$$
Here we need to calculate $a_i$, $b_i$, $c_i$ and $d_i$, where the first is just $y_i$. For the last three we need the derivative of $S(x_i)\equiv S_i$. In the cubic spline interpolation, the derivative is derived from the continuity equation. Here it is a linear combination of the nearest slopes:
$$S'_i = \frac{w_{i+1}p_{i-1} + w_{i-1}p_i}{w_{i+1} + w_{i-1}} if w_{i+1} + w_{i-1} =/ 0,$$
$$S'_i = \frac{p_{i-1} + p_i}{2} if w_{i+1} + w_{i-1} = 0.$$
Where $p_i=\frac{\Delta y_i}{\Delta x_i}$ and $w = |p_i - p_{i-1}|$. Since $S'_i$ depends on its first and second neighbors, the two points on each side's edge have prescribed values. $S'_1 = p_1$, $S'_2 = \frac{p_1 + p_2}{2}$, $S'_{n-1} = \frac{p_{n-1} + p_{n-2}}{2}$ and $S'_n = p_{n-1}$. From these the derivative and thereby the last three parameters; $b_i$, $c_i$ and $d_i$, are determined. 

These interpolations are created with lists of doubles. Therefore, I implemented the 'vector' class from my library. Here the derivative and integral were also implemented, as they were like cubic splines.
## b - Plot for different equations and evaluate the advantages compared to the cubic and quadratic spline interpolation:
The compare the three spline interpolations, three equations were looked at. The first was partly discontinuity with a point in at zero and otherwise being either -0.5 or 0.5. The second was with two discontinuities, which was -0.7, 0.2 and 0.1. The last was a discontinuous bulge on a function. This was created by two different quadratic equations were used. The value chosen here would be the equation that gives the largest. The code that created the quadratic and cubic splines were the one made in the "splines" assignment. Here all three were evaluated on the first two functions. The last function was used to compare the cubic and Akima Sub-Splines. These were then plotted in using Gnuplot.
![Akima Sub-Spline interpolation (ASSI) compared to quadratic and cubic spline interpolation.](comparesplines.png)
In the figures above, the upper figures compare ASSI to the quadratic and cubic spline. For the partly discontinuous function, the quadratic spline is the best fitting. With full discontinuity, the quadratic has large stable oscillations, which can be seen in the second figure. Here the cubic and Akima Sub-Spline are optimal, though the cubic spline still produces oscillation when it goes through the discontinuities. The oscillations come from having a periodic point. This is visualized well in the upper right figure. Where the quadratic oscillations between every point. In the lower figures, these are compared in a function that has a discontinuities bulge. This is created by having two different quadratic equations and using the one with the highest value. The close-up on the last figure shows that the Akima Sub-Spline is optimal for these discontinuities. 
![Akima Sub-Spline with irregular point placement.](nonperiod.png)
This does not change with irregular point placements, as can be seen in the figures above. When the points are irregular, the quadratic and cubic spline interpolations do not work well, as they produce irregular oscillations. For linear equations, the ASSI still works well, but for the more advanced functions it begins to fail, as can be seen in the third figure.
![Akima Sub-Spline at point limit.](limit.png)
The disadvantage of ASSI can be seen in the figures above. If the discontinuity is not surrounded by two points on each side. It the wiggle is still partly produced. The failure comes from the dependence of these neighbors. Therefore, to make the Akima sub-spline interpolation, at least six points are needed, two points making the discontinuity and two on each side of them.
## (c) implement the modified Akima Sub-Spline interpolation (MASSI) and compare them with the original Akima Sub-Spline:
The implementation is done by adding an extra parameter to the program, which when set to one it will give the modified Akima Sub-Spline (MASSI). These are then plotted with a partial discontinuity, as it is what Makima is made for. 
![Modified Akima Sub-Spline compared to the original ASSI.](comparemakima.png)
The left figure shows that the last wiggles are removed with the implementation. This can be seen in the right figure, with the derivatives. Here there is a better visualization for what happens. There is no initial or resulting wiggle and the Makima version moves more vertically than the original Akima Sub-Spline.
# Evaluation
## Points
| a | b | c |total|
|---|---|---|-----|
|6/6|2/3|1/1| 9/10|
## a)
Even though the implementation was not that different from what was done in 'splines' assignment. I will give six points as it is fully implemented and can also give the derivative and integral. It works as well as the cubic spline interpolation class in the assignment.
## b)
The different figures show the advantages and weakness well. They show why we use the Akima sub-spline instead of either quadratic or cubic spline, both with and without periodic point placements. They also show the limit of it and when it stops working. 

The comparison could be optimized by creating an expanded ASSI class, which took a matrix of y values, instead of vector. This would lessen the small recycling of code for creating the interpolations. Therefore, I will not get full points this.
## c)
The implementation works well, although it is activated by setting a parameter to true when using the interpolation. I will still give it one point, as it is still an optimal implementation with only adding additional values into the vector $w$ and the different between it and the original ASSI is displayed and described well.




