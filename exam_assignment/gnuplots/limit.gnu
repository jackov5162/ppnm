set terminal pngcairo size 1200, 600 background "white" ;\
set output "limit.png" ;\
set grid ;\
set xrange [-3:3] ;\
set yrange [-0.7:0.7] ;\
set multiplot layout 1,2 ;\
set title "ASSI: 6 points" ;\
plot \
     'limit.txt' index 1 every ::2 using 1:2 with lines lc "blue" title "Exact result" \
    ,'limit.txt' index 1 every ::2 using 1:3 with lines lc "black" lw 3 dashtype 2 title "ASSI" \
    ,'limit.txt' index 1 every ::2 using 1:4 with lines lc "red" lw 3 dashtype 5 title "Cubic spline interpolation" \
    ,'limit.txt' index 0 every ::2 using 1:2 with points pointtype 3 lw 4 lc "blue" title "points" ;\
unset title ;\
set title "ASSI: 5 points" ;\
plot \
     'limit.txt' index 2 every ::2 using 1:2 with lines lc "blue" notitle \
    ,'limit.txt' index 2 every ::2 using 1:3 with lines lc "black" lw 3 dashtype 2 notitle \
    ,'limit.txt' index 2 every ::2 using 1:4 with lines lc "red" lw 3 dashtype 5 notitle \
    ,'limit.txt' index 0 every ::3::7 using 1:2 with points pointtype 3 lw 4 lc "blue" notitle ;\
