set terminal pngcairo size 1200, 600 background "white" ;\
set output "comparemakima.png" ;\
set grid ;\
set xrange [-1:1] ;\
set yrange [-0.8:0.8] ;\
set multiplot layout 1,2 ;\
set title "MASSI and ASSI: Partial discontinoity" ;\
plot \
     'compare.txt' index 2 every ::2 using 1:2 with lines lc "blue" title "Exact result" \
    ,'compare.txt' index 1 every ::2 using 1:2 with lines lc "black" lw 3 dashtype 2 title "ASSI" \
    ,'compare.txt' index 3 every ::2 using 1:2 with lines lc "red" lw 3 dashtype 2 title "Modified ASSI" \
    ,'compare.txt' index 0 every ::2 using 1:2 with points pointtype 3 lw 4 lc "blue" title "points" ;\
unset title ;\
unset yrange ;\
set yrange [-2.5:0.5] ;\
set title "MASSI and ASSI: derivative " ;\
plot \
    'compare.txt' index 1 every ::2 using 1:3 with lines lc "black" lw 3 dashtype 2 notitle \
    ,'compare.txt' index 3 every ::2 using 1:3 with lines lc "red" lw 3 dashtype 2 notitle ;\
