set terminal pngcairo size 1200, 1200 background "white" ;\
set output "nonperiod.png" ;\
set grid ;\
set xrange [-3:3] ;\
set yrange [-1.5:1.5] ;\
set multiplot layout 2,2 ;\
set title "ASSI: Irregular point placement with a single discontinuity" ;\
plot \
     'nonperiod.txt' index 1 every ::2 using 1:2 with lines lc "blue" title "Exact result" \
    ,'nonperiod.txt' index 1 every ::2 using 1:3 with lines lc "black" lw 3 dashtype 2 title "ASSI" \
    ,'nonperiod.txt' index 1 every ::2 using 1:4 with lines lc "orange" lw 3 dashtype 4 title "Quadratic spline interpolation" \
    ,'nonperiod.txt' index 1 every ::2 using 1:5 with lines lc "red" lw 3 dashtype 5 title "Cubic spline interpolation" \
    ,'nonperiod.txt' index 0 every ::2 using 1:2 with points pointtype 3 lw 4 lc "blue" title "points" ;\
unset xrange ;\
unset yrange ;\
unset title ;\
set xrange [-3:3] ;\
set yrange [-1:1] ;\
set title "ASSI: Irregular point placement with multiple discontinuities" ;\
plot \
     'nonperiod.txt' index 1 every ::2 using 1:6 with lines lc "blue" notitle \
    ,'nonperiod.txt' index 1 every ::2 using 1:7 with lines lc "black" lw 3 dashtype 2 notitle \
    ,'nonperiod.txt' index 1 every ::2 using 1:8 with lines lc "orange" lw 3 dashtype 4 notitle \
    ,'nonperiod.txt' index 1 every ::2 using 1:9 with lines lc "red" lw 3 dashtype 5 notitle \
    ,'nonperiod.txt' index 0 every ::2 using 1:3 with points pointtype 3 lw 4 lc "blue" notitle ;\
unset xrange ;\
unset yrange ;\
unset title ;\
set xrange [-5:0] ;\
set yrange [2.5:8.5] ;\
set title "ASSI: Irregular point placement function with discontinoities" ;\
plot \
     'nonperiod.txt' index 1 every ::2 using 1:10 with lines lc "blue" notitle \
    ,'nonperiod.txt' index 1 every ::2 using 1:11 with lines lc "black" lw 3 dashtype 2 notitle \
    ,'nonperiod.txt' index 1 every ::2 using 1:12 with lines lc "red" lw 3 dashtype 5 notitle \
    ,'nonperiod.txt' index 0 every ::2 using 1:4 with points pointtype 3 lw 4 lc "blue" notitle ;\
unset xrange ;\
unset yrange ;\
unset title ;\
set xrange [-2:0] ;\
set yrange [2.5:4] ;\
set title "Close-up" ;\
plot \
     'nonperiod.txt' index 1 every ::2 using 1:10 with lines lc "blue" notitle \
    ,'nonperiod.txt' index 1 every ::2 using 1:11 with lines lc "black" lw 3 dashtype 2 notitle \
    ,'nonperiod.txt' index 1 every ::2 using 1:12 with lines lc "red" lw 3 dashtype 5 notitle \
    ,'nonperiod.txt' index 0 every ::2 using 1:4 with points pointtype 3 lw 4 lc "blue" notitle ;\
