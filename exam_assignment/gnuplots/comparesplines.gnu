set terminal pngcairo size 1200, 1200 background "white" ;\
set output "comparesplines.png" ;\
set grid ;\
set xrange [-1:2] ;\
set yrange [-0.7:0.7] ;\
set multiplot layout 2,2 ;\
set title "ASSI comparsion: partial discontinoity" ;\
plot \
     'compare.txt' index 2 every ::2 using 1:2 with lines lc "blue" title "Exact result" \
    ,'compare.txt' index 1 every ::2 using 1:2 with lines lc "black" lw 3 dashtype 2 title "ASSI" \
    ,'compare.txt' index 2 every ::2 using 1:3 with lines lc "orange" lw 3 dashtype 4 title "Quadratic spline interpolation" \
    ,'compare.txt' index 2 every ::2 using 1:4 with lines lc "red" lw 3 dashtype 5 title "Cubic spline interpolation" \
    ,'compare.txt' index 0 every ::2 using 1:2 with points pointtype 3 lw 4 lc "blue" title "points" ;\
unset title ;\
unset xrange ;\
unset yrange ;\
set xrange [-3:3] ;\
set yrange [-1:1] ;\
set title "ASSI comparsion: Multiple discontinoities" ;\
plot \
     'compare.txt' index 2 every ::2 using 1:5 with lines lc "blue" notitle \
    ,'compare.txt' index 1 every ::2 using 1:5 with lines lc "black" lw 3 dashtype 2 notitle \
    ,'compare.txt' index 2 every ::2 using 1:6 with lines lc "orange" lw 3 dashtype 4 notitle \
    ,'compare.txt' index 2 every ::2 using 1:7 with lines lc "red" lw 3 dashtype 5 notitle \
    ,'compare.txt' index 0 every ::2 using 1:3 with points pointtype 3 lw 4 lc "blue" notitle ;\
unset title ;\
unset xrange ;\
unset yrange ;\
set xrange [-5:0] ;\
set yrange [2.5:8.5] ;\
set title "ASSI comparsion: Discontinoues bulge" ;\
plot \
     'compare.txt' index 2 every ::2 using 1:8 with lines lc "blue" notitle \
    ,'compare.txt' index 1 every ::2 using 1:8 with lines lc "black" lw 3 dashtype 2 notitle \
    ,'compare.txt' index 2 every ::2 using 1:9 with lines lc "red" lw 3 dashtype 5 notitle \
    ,'compare.txt' index 0 every ::2 using 1:4 with points pointtype 3 lw 4 lc "blue" notitle ;\
unset xrange ;\
unset yrange ;\
set xrange [-2:0] ;\
set yrange [2.5:4] ;\
set title "Close-up of bulge: right side" ;\
plot \
     'compare.txt' index 2 every ::2 using 1:8 with lines lc "blue" notitle \
    ,'compare.txt' index 1 every ::2 using 1:8 with lines lc "black" lw 3 dashtype 2 notitle \
    ,'compare.txt' index 2 every ::2 using 1:9 with lines lc "red" lw 3 dashtype 5 notitle \
    ,'compare.txt' index 0 every ::2 using 1:4 with points pointtype 3 lw 4 lc "blue" notitle ;\
