using System;
using static System.Console;
using static System.Math;
using System.IO;

public class nonperiod{
    public static void Main(){
        int i;
        double a1 = 0.1;
        double a2 = -2;
        double b = 3;

        double c1 = -5;
        double c2 = 0;
        
        vector x = new vector(-5.6, -5.4, -5.2, -4.5, -4.1, -3.4, -3.1, -2.9, -2.0, -1.5, -1.25, -0.9, -0.2, 0.1, 0.2, 0.9, 1.2, 1.4, 2.3, 3.2);
        matrix y = new matrix(x.size, 3);

        int steps = 100*x.size;


        Func<double,double> single = xs =>{
            if(xs < 0) return 0.5;
            if(xs > 0) return -0.5;
            else{
                return 0;
            };
        };

        Func<double,double> multiple = xs =>{
            if(xs < -1) return -0.7;
            if(xs >= -1) {
                if(xs < 1) return 0.2;
                else{ return -0.1;}
            }
            else{
                return -0.1;
            }
        };        

        Func<double,double> bulge = xs =>{
            double ys = Pow(xs, 2) * a1 + b;
            double y1 = Pow(xs - (c2 + c1)/2, 2) * a2 + b * 2;
            if(ys < y1) ys = y1 + 2;
            return ys;
        };        

        for(i = 0; i < x.size; i++){
            y[0][i] = single(x[i]);
            y[1][i] = multiple(x[i]);
            y[2][i] = bulge(x[i]);
        }



        akimasubs akimaS = new akimasubs(x, y[0]);
        akimasubs akimaM = new akimasubs(x, y[1]);
        akimasubs akimaB = new akimasubs(x, y[2]);
        qspline QsplineS = new qspline(x, y[0]);
        qspline QsplineM = new qspline(x, y[1]);
        cspline CsplineS = new cspline(x, y[0]);
        cspline CsplineM = new cspline(x, y[1]);
        cspline CsplineB = new cspline(x, y[2]);
        double z, step = (x[x.size - 1] - x[0]) / (steps - 1);

        Write($"Points\n");
        Write($"x y\n");
        for(i = 0; i < x.size; i++){
            WriteLine($"{x[i]} {y[0][i]} {y[1][i]} {y[2][i]}");
        }
        Write($"\n");
        Write($"\n");
        Write($"x Single(x) Single-Akima(x) Single-Quadratic(x) Single-Cubic(x) Multiple(x) Multiple-Akima(x) Multiple-Quadratic(x) Multiple-Cubic(x) Bulge(x) Bulge-Akima(x) Bulge-Cubic(x)\n");
        for(z = x[0], i = 0; i < steps - 1; z = x[0]+(++i) * step){
            Write($"{z} {single(z)} {akimaS.evaluate(z)} {QsplineS.evaluate(z)} {CsplineS.evaluate(z)} ");
            Write($"{multiple(z)} {akimaM.evaluate(z)} {QsplineM.evaluate(z)} {CsplineM.evaluate(z)} ");
            WriteLine($"{bulge(z)} {akimaB.evaluate(z)} {CsplineB.evaluate(z)}");
        }
    }
}