using System;
using static System.Console;
using static System.Math;
using System.IO;

public class compare{
    public static void Main(){
        int start = -10;
        int stepdivide = 3;
        int amount = 2 * Abs(start) * stepdivide + 1;
        int steps = 1200;
        int i;

        double a1 = 0.1;
        double a2 = -2;
        double b = 3;

        double c1 = -5;
        double c2 = 0;
        
        vector x = new vector(amount);
        matrix y = new matrix(amount, 3);
        
        Func<double,double> single = xs =>{
            if(xs < 0) return 0.5;
            if(xs > 0) return -0.5;
            else{
                return 0;
            };
        };

        Func<double,double> multiple = xs =>{
            if(xs < -1) return -0.7;
            if(xs >= -1) {
                if(xs < 1) return 0.2;
                else{ return -0.1;}
            }
            else{
                return -0.1;
            }
        };        

        Func<double,double> bulge = xs =>{
            double ys = Pow(xs, 2) * a1 + b;
            double y1 = Pow(xs - (c2 + c1)/2, 2) * a2 + b * 2;
            if(ys < y1) ys = y1 + 2;
            return ys;
        };        

        for(i = 0; i < amount; i++){
            double stepsize = stepdivide;
            x[i] = start + i / stepsize;
            y[0][i] = single(x[i]);
            y[1][i] = multiple(x[i]);
            y[2][i] = bulge(x[i]);
        }

        

        akimasubs akimaS = new akimasubs(x, y[0]);
        akimasubs akimaM = new akimasubs(x, y[1]);
        akimasubs akimaB = new akimasubs(x, y[2]);
        akimasubs makimaS = new akimasubs(x, y[0], true);
        akimasubs makimaM = new akimasubs(x, y[1], true);
        akimasubs makimaB = new akimasubs(x, y[2], true);
        qspline QsplineS = new qspline(x, y[0]);
        qspline QsplineM = new qspline(x, y[1]);
        cspline CsplineS = new cspline(x, y[0]);
        cspline CsplineM = new cspline(x, y[1]);
        cspline CsplineB = new cspline(x, y[2]);
        double z, step = (x[amount - 1] - x[0]) / (steps - 1);

        Write($"Points\n");
        Write($"x single(x) multiple(x) bulge(x)\n");
        for(i = 0; i < x.size; i++){
            WriteLine($"{x[i]} {y[0][i]} {y[1][i]} {y[2][i]}");
        }
        Write($"\n");
        Write($"\n");
        Write($"Akima Sub-Spline\n");
        Write($"x Single-Akima(x) Single-Akima-Der(x) Single-Akima-Int(x) Multiple-Akima(x) Multiple-Akima-Der(x) Multiple-Akima-Int(x) Bulge-Akima(x) Bulge-Akima-Der(x) Bulge-Akima-Int(x)\n");
        for(z = x[0], i = 0; i < steps - 1; z = x[0]+(++i) * step){
            Write($"{z} ");
            Write($"{akimaS.evaluate(z)} {akimaS.derivative(z)} {akimaS.integral(z)} ");
            Write($"{akimaM.evaluate(z)} {akimaM.derivative(z)} {akimaM.integral(z)} ");
            WriteLine($"{akimaB.evaluate(z)} {akimaB.derivative(z)} {akimaB.integral(z)}");
        }
        Write($"\n");
        Write($"\n");
        Write($"Spline comparison\n");
        Write($"x Single(x) Single-Quad(x) Single-Cubic(x) Multiple(x) Multiple-Quad(x) Multiple-Cubic(x) Bulge(x) Bulge-Cubic(x)\n");
        for(z = x[0], i = 0; i < steps - 1; z = x[0]+(++i) * step){
            Write($"{z} ");
            Write($"{single(z)} {QsplineS.evaluate(z)} {CsplineS.evaluate(z)} ");
            Write($"{multiple(z)} {QsplineM.evaluate(z)} {CsplineM.evaluate(z)} ");
            WriteLine($"{bulge(z)} {CsplineB.evaluate(z)}");
        }
        Write($"\n");
        Write($"\n");
        Write($"Modified Akima Sub-Spline (Makima)\n");
        Write($"x Single-Makima(x) Single-Makima-Der(x) Single-Makima-Int(x) Multiple-Makima(x) Multiple-Makima-Der(x) Multiple-Makima-Int(x) Bulge-Makima(x) Bulge-Makima-Der(x) Bulge-Makima-Int(x)\n");
        for(z = x[0], i = 0; i < steps - 1; z = x[0]+(++i) * step){
            Write($"{z} ");
            Write($"{makimaS.evaluate(z)} {makimaS.derivative(z)} {makimaS.integral(z)} ");
            Write($"{makimaM.evaluate(z)} {makimaM.derivative(z)} {makimaM.integral(z)} ");
            WriteLine($"{makimaB.evaluate(z)} {makimaB.derivative(z)} {makimaB.integral(z)}");
        }        
    }
}