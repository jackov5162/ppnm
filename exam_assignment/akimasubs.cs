using System;
using static System.Math;
using static System.Console;

public class akimasubs{
    public vector xs, ys, a, b, c, d, dx, dy, p, w, S;
    double h;
    int n, bin;
    public akimasubs(vector x, vector y, bool makima=false){
        xs = x.copy();
        ys = y.copy();
        n = xs.size;
        p = new vector(n - 1);
        dx = new vector(n - 1);
        dy = new vector(n - 1);
        for(int i = 0; i < n - 1; i++){
            dx[i] = xs[i + 1] - xs[i];
            dy[i] = ys[i + 1] - ys[i];
            p[i] = dy[i] / dx[i];
        }
        w = new vector(p.size - 1);
        for(int i = 1; i < w.size; i++){
            w[i] = Abs(p[i] - p[i - 1]);
        }
        if(makima){
            for(int i = 1; i < w.size; i++){
                w[i] += Abs(p[i] + p[i - 1]) / 2;
            }
        }
        S = new vector(n);
        S[0] = p[0];
        S[1] = (p[0] + p[1]) / 2;
        S[n - 2] = (p[n - 2] + p[n - 3]) / 2;
        S[n - 1] = p[n - 2];
        for(int i = 2; i < n - 3; i++){
            double j = w[i + 1] + w[i - 1];
            if(j != 0){
                S[i] = (w[i + 1] * p[i - 1] + w[i - 1] * p[i]) / (w[i + 1] + w[i - 1]);
            }
            else{
                S[i] = (p[i - 1] + p[i]) / 2;
            }
        }
        a = y.copy();
        b = S.copy();
        c = new vector(n - 1);
        d = new vector(n - 1);
        for(int i = 0; i < n - 1; i++){
            c[i] = (3 * p[i] - 2 * S[i] - S[i + 1]) / dx[i];
            d[i] = (S[i] + S[i + 1] - 2 * p[i]) / (dx[i] * dx[i]);
        }
    }

    public void binsearch(double z){
        if(z < xs[0] || z > xs[n - 1]){
            throw new Exception("binsearch: bad z");
        }
        bin = 0;
        int j = n - 1;
        while(j - bin > 1){
            int mid = (bin + j) / 2;
            if(z > xs[mid]){
                bin = mid;
            }
            else{
                j = mid;
            }
        }
    }

    public double evaluate(double z){
        binsearch(z);
        h = z - xs[bin];
        return a[bin] + b[bin] * h + c[bin] * h*h + d[bin] * h*h*h;
    }
    public double derivative(double z){
        binsearch(z);
        h = z - xs[bin];
        return b[bin] + 2 * c[bin] * h + 3 * d[bin] * h*h;
    }
    public double integral(double z){
        binsearch(z);
        double sum = 0;
        h = z - xs[bin];
        for(int i =0; i < bin; i++){
            h = xs[i + 1] - xs[i];
            sum += h * a[i] + Pow(h, 2) / 2 * b[i] + Pow(h, 3) / 3 * c[i] + Pow(h, 4) / 4 * d[i];
        }
        h = z - xs[bin];
        sum += h * a[bin] + Pow(h, 2) / 2 * b[bin] + Pow(h, 3) / 3 * c[bin] + Pow(h, 4) / 4 * d[bin];
        return sum;
    }
}