using System;
using static System.Console;
using static System.Math;
using System.IO;

public class limit{
    public static void Main(){
        int steps = 1200;
        int i;
        
        vector x = new vector(-2.5, -1.5, -0.5, 0.5, 1.5, 2.5);
        vector xlimit = new vector(-1.5, -0.5, 0.5, 1.5, 2.5);
        vector y = new vector(x.size);
        vector ylimit = new vector(xlimit.size);
        
        Func<double,double> func = xs =>{
            if(xs < 0) return 0.5;
            if(xs > 0) return -0.5;
            else{
                return 0;
            };
        };

        for(i = 0; i < x.size; i++){
            y[i] = func(x[i]);
            if(i < xlimit.size){
                ylimit[i] = func(xlimit[i]);
            };
        }


        akimasubs akima = new akimasubs(x, y);
        akimasubs akimalimit = new akimasubs(xlimit, ylimit);
        cspline Cspline = new cspline(x, y);
        cspline Csplinelimit = new cspline(xlimit, ylimit);
        double z, step = (x[x.size - 1] - x[0]) / (steps - 1);
        double zlimit, steplimit = (xlimit[xlimit.size - 1] - xlimit[0]) / (steps - 1);

        Write($"Points\n");
        Write($"x y\n");
        for(i = 0; i < x.size; i++){
            WriteLine($"{x[i]} {y[i]}");
        }
        Write($"\n");
        Write($"\n");
        Write($"Successful:\n");
        Write($"x exact(x) Akima(x) Cubic(x)\n");
        for(z = x[0], i = 0; i < steps - 1; z = x[0]+(++i) * step){
            WriteLine($"{z} {func(z)} {akima.evaluate(z)} {Cspline.evaluate(z)}");
        }
        Write($"\n");
        Write($"\n");
        Write($"Failed:\n");
        Write($"x exact(x) Akima(x) Cubic(x)\n");
        for(zlimit = xlimit[0], i = 0; i < steps - 1; zlimit = xlimit[0]+(++i) * steplimit){
            WriteLine($"{zlimit} {func(zlimit)}  {akimalimit.evaluate(zlimit)} {Csplinelimit.evaluate(zlimit)}");
        }
    }
}